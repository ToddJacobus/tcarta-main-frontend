import uuid from 'react-uuid';

import getContent from "../apis/getContent";
import sendEmailApi from '../apis/sendEmail';
import submitContact from '../apis/submitContact'

const API_TOKEN = 'BYHioCKg48x2jyBzTyZoSN7A4dEa4nhnchRR3gNDkRI' // production api token

const USER_ID = "user_NoZKcMSWuY9kmVhdQZrfZ"
const TEMPLATE_ID = "template_e1b7Teb7"
const SERVICE_ID = "contact_tcarta"

//  --  SENDING CONTACT ACTIONS

export const sendContact = props => async dispatch => {
    const response = await submitContact.post('/contact/contacts/', {
        source: 'tcarta.com',
        ...props,
    })
    dispatch({type: 'SEND_CONTACT', payload: response.data})
};


//  --  SENDING EMAIL ACTIONS

export const sendEmail = (props) => async dispatch => {
    const response = await sendEmailApi.post('/send', {
        user_id: USER_ID,
        template_id: TEMPLATE_ID,
        service_id: SERVICE_ID,
        template_params: { ...props, uuid: uuid() }
    })
    dispatch({type: 'SEND_EMAIL', payload: response.data})
}

//  --  CONTENT FETCHING ACTIONS

export const fetchHeroPageCarouselContent = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=heroPageCarousel`)
    dispatch({type: 'GET_HERO_CAROUSEL', payload: response.data})
};

export const fetchProductStories = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=productStory`)
    dispatch({type: 'GET_PRODUCT_STORIES', payload: response.data})
};

export const fetchProfilePhotos = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=profilePhoto`)
    dispatch({type: 'GET_PROFILES', payload: response.data})
};

export const fetchBlogPosts = (props) => async dispatch => {
    const { entryId, slug, limit, skip } = props ? props : {};
    const entryIdQuery = entryId ? `&sys.id[in]=${props.entryId}` : '';
    const limitQuery = limit ? `&limit=${limit}`: '';
    const skipQuery = skip ? `&skip=${skip}` : '';
    const slugQuery = slug ? `&fields.slug[in]=${slug}` : '';
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=blogPost${entryIdQuery}${limitQuery}${skipQuery}${slugQuery}`)
    dispatch({type: 'GET_BLOG_POSTS', payload: response.data})
};

export const fetchPressReleases = (props) => async dispatch => {
    const { entryId, slug, limit, skip } = props ? props : {};
    const entryIdQuery = entryId ? `&sys.id[in]=${props.entryId}` : '';
    const limitQuery = limit ? `&limit=${limit}`: '';
    const skipQuery = skip ? `&skip=${skip}` : '';
    const slugQuery = slug ? `&fields.slug[in]=${slug}` : '';
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=pressRelease${entryIdQuery}${limitQuery}${skipQuery}${slugQuery}`)
    dispatch({type: 'GET_PRESS_RELEASES', payload: response.data})
};

export const fetchPartnershipLogos = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=partnershipLogos`)
    dispatch({type: 'GET_PARTNERSHIP_LOGOS', payload: response.data})
};

export const fetchCapabilitiesGif = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=capabilitiesGif`)
    dispatch({type: 'GET_CAPABILITIES_GIF', payload: response.data})
};

export const fetchCapabilitiesExamples = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=capabilitiesExamples`)
    dispatch({type: 'GET_CAPABILITIES_EXAMPLES', payload: response.data})
};

export const fetchWordClouds = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=wordClouds`)
    dispatch({type: 'GET_WORD_CLOUDS', payload: response.data})
};

export const fetchGrantOrgs = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=grantingOrganizations`)
    dispatch({type: 'GET_GRANT_ORGS', payload: response.data})
};

export const fetchBlogBannerImage = (props) => async dispatch => {
    const { entryId } = props ? props : {};
    const entryIdQuery = entryId ? `&sys.id[in]=${props.entryId}` : '';
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=blogPageBannerImage${entryIdQuery}`)
    dispatch({type: 'GET_BLOG_BANNER', payload: response.data})
};

export const fetchLegalDocuments = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=legalDocuments`)
    dispatch({type: 'GET_LEGAL_DOCUMENTS', payload: response.data})
};

export const fetchGalleryImages = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=galleryImages`)
    dispatch({type: 'GET_GALLERY_IMAGES', payload: response.data})
};

export const fetchEvents = (props) => async dispatch => {
    const { slug } = props ? props : {};
    const slugQuery = slug ? `&fields.slug[in]=${props.slug}` : '';
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=event${slugQuery}`)
    dispatch({type: 'GET_EVENTS', payload: response.data})
};

export const fetchFutureEvents = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=futureEvents`)
    dispatch({type: 'GET_FUTURE_EVENTS', payload: response.data})
};

export const fetchEventPromotions = (props) => async dispatch => {
    const { eventId } = props ? props : {};
    const eventQuery = eventId ? `&fields.eventId[in]=${eventId}`: ''
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=eventPromotion${eventQuery}`)
    dispatch({type: 'GET_EVENT_PROMOTIONS', payload: response.data})
};

export const fetchKidsCornerPosts = (props) => async dispatch => {
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=kidsCornerPost`)
    dispatch({type: 'GET_KIDS_CORNER_POSTS', payload: response.data})
};

export const fetchEventDetailitems = (props) => async dispatch => {
    const { slug } = props ? props : {};
    const slugQuery = slug ? `&fields.eventSlug[in]=${props.slug}` : '';
    const response = await getContent.get(`?access_token=${API_TOKEN}&content_type=eventDetailItem${slugQuery}`)
    dispatch({type: 'GET_EVENT_DETAIL_ITEMS', payload: response.data})
};