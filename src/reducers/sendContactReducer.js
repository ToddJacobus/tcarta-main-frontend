export default (state = null, action) => {
    switch (action.type) {
        case "SEND_CONTACT":
            return action.payload
        default:
            return state
    };
};