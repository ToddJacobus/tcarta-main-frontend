// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_HERO_CAROUSEL":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.carouselImage && action.payload.includes.Asset.find(
                            asset => 
                                asset.sys.id === item.fields.carouselImage.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            return {
                                order: item.fields.order,
                                tagline: item.fields.carouselTagline,
                                link: item.fields.url,
                                // find the linked image for the given content entry
                                url: action.payload.includes && item.fields.carouselImage ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.carouselImage.sys.id 
                                    ).fields.file.url : undefined,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}