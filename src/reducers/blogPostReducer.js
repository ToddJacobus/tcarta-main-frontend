// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_BLOG_POSTS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.heroImage && action.payload.includes.Asset.find(
                            asset => asset.sys.id === item.fields.heroImage.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            const dateObj = new Date(item.fields.publishDate)
                            return {
                                id: item.sys.id,
                                body: item.fields.body,
                                title: item.fields.title,
                                description: item.fields.description,
                                author: item.fields.author,
                                authorTitle: item.fields.authorTitle,
                                actionTitle: item.fields.actionTitle,
                                actionLink: item.fields.actionLink,
                                publishDate: `${dateObj.toLocaleString('default', { month: 'long' })} ${dateObj.getDate()}, ${dateObj.getFullYear()}`,
                                publishDateObj: dateObj,
                                slug: item.fields.slug,
                                // find the linked image for the given content entry
                                url: action.payload.includes && item.fields.heroImage ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.heroImage.sys.id 
                                    ).fields.file.url : undefined, 
                            }
                        }).sort((a,b) => b.publishDateObj - a.publishDateObj)
                    ],
                    linkedAssets: action.payload.includes.Asset,
                    total: action.payload.total,
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}