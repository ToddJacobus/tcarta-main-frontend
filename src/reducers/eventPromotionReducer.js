// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_EVENT_PROMOTIONS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if ( 
                                item.fields.image &&
                                action.payload.includes.Asset.find(
                                asset => 
                                    asset.sys.id === item.fields.image.sys.id
                                )
                            ) {
                                return true
                            } else {
                                return false
                            }
                    }
                );
                
                return {
                    items:[
                        ...items.map(item => {
                            const publicationDate = new Date(item.fields.publicationDate)
                            return {
                                id: item.sys.id,
                                title: item.fields.title,
                                description: item.fields.description,
                                link: item.fields.link,
                                order: item.fields.order,
                                eventId: item.fields.eventId,
                                publicationDate: publicationDate,
                                publicationDateString: `${publicationDate.toLocaleString('default', { month: 'long' })} ${publicationDate.getDate()}, ${publicationDate.getFullYear()}`,

                                // find the linked image for the given content entry
                                image: action.payload.includes && item.fields.image ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.image.sys.id 
                                    ).fields.file.url : undefined, 
                              
                            }
                        }).sort((a,b) => b.publicationDate - a.publicationDate)
                    ],
                    total: action.payload.total,
                    linkedAssets: action.payload.includes.Asset,
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}