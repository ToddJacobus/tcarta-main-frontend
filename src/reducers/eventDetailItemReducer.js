// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_EVENT_DETAIL_ITEMS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if ( 
                                item.fields.header &&
                                item.fields.body &&
                                item.fields.eventSlug
                            ) {
                                return true
                            } else {
                                return false
                            }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            const dateObj = item.fields.date ? new Date(item.fields.date) : undefined;
                            return {
                                id: item.sys.id,
                                header: item.fields.header,
                                subHeader: item.fields.subHeader,
                                body: item.fields.body,
                                order: item.fields.order,
                                slug: item.fields.slug,
                                dateObj: dateObj,
                                date: dateObj ? `${dateObj.toLocaleString('default', { month: 'long' })} ${dateObj.getDate()}, ${dateObj.getFullYear()}` : undefined,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ],
                    total: action.payload.total,
                    linkedAssets: action.payload.includes ?
                                    action.payload.includes.Asset
                                    : undefined,
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}