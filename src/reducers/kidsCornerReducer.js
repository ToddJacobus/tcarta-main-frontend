// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_KIDS_CORNER_POSTS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                // filter out items that are missing required assets
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.image && action.payload.includes.Asset.find(
                            asset => 
                                asset.sys.id === item.fields.image.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items: [
                        ...items.map(item => {
                            const dateObj = new Date(item.fields.publicationDate)
                            // Images are required, so if there's no assotiated image, return nothing
                            return {
                                id: item.sys.id,
                                title: item.fields.title,
                                link: item.fields.link,
                                longLink: item.fields.longLink,
                                content: item.fields.content,
                                publicationDate: `${dateObj.toLocaleString('default', { month: 'long' })} ${dateObj.getDate()}, ${dateObj.getFullYear()}`,
                                publicationDateObj: dateObj,
                                featured: item.fields.featured,
                                // find the linked image for the given content entry
                                url: action.payload.includes && item.fields.image ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.image.sys.id 
                                    ).fields.file.url : undefined,
                            }
                        }).sort((a,b) => b.publicationDateObj - a.publicationDateObj)
                    ],
                    assets: action.payload.includes.Asset,
                    total: action.payload.total,
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}