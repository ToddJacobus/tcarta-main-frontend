// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_EVENTS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if ( 
                                item.fields.introVideo &&
                                item.fields.bannerImage &&
                                item.fields.heroImage && 
                                action.payload.includes.Asset.find(
                                asset => 
                                    asset.sys.id === item.fields.introVideo.sys.id ||
                                    asset.sys.id === item.fields.bannerImage.sys.id ||
                                    asset.sys.id === item.fields.heroImage.sys.id
                                )
                            ) {
                                return true
                            } else {
                                return false
                            }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            // const pubDateObj = new Date(item.fields.publicationDate)
                            const startDateObj = new Date(item.fields.startDate)
                            const endDateObj = new Date(item.fields.endDate)
                            return {
                                id: item.sys.id,
                                title: item.fields.title,
                                subTitle: item.fields.subTitle,
                                introVideoCaption: item.fields.introVideoCaption,
                                startDate: `${startDateObj.toLocaleString('default', { month: 'long' })} ${startDateObj.getDate()}`,
                                startDateObj: startDateObj,
                                endDate: `${endDateObj.toLocaleString('default', { month: 'long' })} ${endDateObj.getDate()}, ${endDateObj.getFullYear()}`,
                                endDateObj: endDateObj,
                                dateText: item.fields.dateText,
                                description: item.fields.description,
                                actionDescription: item.fields.actionDescription,
                                calendlyUrl: item.fields.calendlyUrl,
                                slug: item.fields.slug,
                                presentationMaterials: item.fields.presentationMaterials,

                                // find the linked image for the given content entry
                                introVideoUrl: action.payload.includes && item.fields.introVideo ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.introVideo.sys.id 
                                    ).fields.file.url : undefined, 
                                bannerImageUrl: action.payload.includes && item.fields.bannerImage ? action.payload.includes.Asset.find( 
                                    asset => asset.sys.id === item.fields.bannerImage.sys.id 
                                ).fields.file.url : undefined,
                                posterImageUrl: action.payload.includes && item.fields.posterImage ? action.payload.includes.Asset.find( 
                                    asset => asset.sys.id === item.fields.posterImage.sys.id 
                                ).fields.file.url : undefined,
                                heroImageUrl: action.payload.includes && item.fields.heroImage ? action.payload.includes.Asset.find( 
                                    asset => asset.sys.id === item.fields.heroImage.sys.id 
                                ).fields.file.url : undefined,
                            }
                        }).sort((a,b) => b.startDateObj - a.startDateObj)
                    ],
                    total: action.payload.total,
                    linkedAssets: action.payload.includes.Asset,
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}