// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_WORD_CLOUDS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if ( item.fields.wordCloud && 
                             item.fields.actionPhoto && 
                             action.payload.includes.Asset.find(
                            asset => 
                                asset.sys.id === item.fields.wordCloud.sys.id ||
                                asset.sys.id === item.fields.actionPhoto.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            return {
                                order: item.fields.order,
                                description: item.fields.description,
                                // find the linked image for the given content entry
                                wordCloudUrl: action.payload.includes && item.fields.wordCloud ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.wordCloud.sys.id 
                                    ).fields.file.url : undefined,
                                actionPhotoUrl: action.payload.includes && item.fields.actionPhoto ? action.payload.includes.Asset.find( 
                                    asset => asset.sys.id === item.fields.actionPhoto.sys.id 
                                    ).fields.file.url : undefined,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}