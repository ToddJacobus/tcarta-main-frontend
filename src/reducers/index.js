import { combineReducers } from 'redux';

import heroCarouselReducer from './heroCarouselReducer';
import productStoryReducer from './productStoryReducer';
import profilePhotosReducer from './profilePhotosReducer';
import blogPostReducer from './blogPostReducer';
import partnershipLogoReducer from './partnershipLogoReducer';
import pressReleaseReducer from './pressReleaseReducer';
import capabilitiesGifReducer from './capabilitiesGifReducer';
import capabilitiesExamplesReducer from './capabilitiesExamplesReducer';
import wordCloudReducer from './wordCloudReducer';
import grantOrgsReducer from './grantOrgsReducer';
import blogBannerReducer from './blogBannerReducer';
import sendEmailReducer from './sendEmailReducer';
import legalDocumentsReducer from './legalDocumentsReducer';
import galleryImagesReducer from './galleryImagesReducer';
import eventsReducer from './eventsReducer';
import futureEventsReducer from './futureEventsReducer';
import eventPromotionReducer from './eventPromotionReducer';
import kidsCornerReducer from './kidsCornerReducer';
import eventDetailItemReducer from './eventDetailItemReducer';
import sendContactReducer from './sendContactReducer';

export default combineReducers({
    hero_carousel_content: heroCarouselReducer,
    product_story_content: productStoryReducer,
    profile_photos_content: profilePhotosReducer,
    blog_posts_content: blogPostReducer,
    partnership_logos_content: partnershipLogoReducer,
    press_releases_content: pressReleaseReducer,
    capabilities_gif_content: capabilitiesGifReducer,
    capabilities_examples_content: capabilitiesExamplesReducer,
    word_cloud_content: wordCloudReducer,
    grant_orgs_content: grantOrgsReducer,
    blog_banner_content: blogBannerReducer,
    send_email_response: sendEmailReducer,
    legal_documents_content: legalDocumentsReducer,
    gallery_images_content: galleryImagesReducer,
    events_content: eventsReducer,
    future_events_content: futureEventsReducer,
    event_promotion_content: eventPromotionReducer,
    kids_corner_content: kidsCornerReducer,
    event_details_content: eventDetailItemReducer,
    send_contact_response: sendContactReducer,
});