// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_FUTURE_EVENTS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                return {
                    items:[
                        ...action.payload.items.map(item => {
                            // const pubDateObj = new Date(item.fields.publicationDate)
                            const startDateObj = new Date(item.fields.startDate)
                            const endDateObj = new Date(item.fields.endDate)
                            return {
                                id: item.sys.id,
                                title: item.fields.title,
                                description: item.fields.description,
                                startDate: `${startDateObj.toLocaleString('default', { month: 'long' })} ${startDateObj.getDate()}`,
                                startDateObj: startDateObj,
                                endDate: `${endDateObj.toLocaleString('default', { month: 'long' })} ${endDateObj.getDate()}, ${endDateObj.getFullYear()}`,
                                endDateObj: endDateObj,
                            }
                        }).sort((a,b) => b.startDateObj - a.startDateObj)
                    ],
                    total: action.payload.total,
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}