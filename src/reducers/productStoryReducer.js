// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_PRODUCT_STORIES":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.heroImage && action.payload.includes.Asset.find(
                            asset => 
                                asset.sys.id === item.fields.heroImage.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            return {
                                header: item.fields.header,
                                subHeader: item.fields.subHeader,
                                // description: item.fields.description,
                                richDescription: item.fields.richDescription,
                                storyMapUrl: item.fields.storyMapUrl,
                                order: item.fields.order,
                                // find the linked image for the given content entry
                                heroImage: item.fields.heroImage && action.payload.includes ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.heroImage.sys.id 
                                    ).fields.file.url : undefined,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}