// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_GRANT_ORGS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.logo && action.payload.includes.Asset.find(
                            asset => 
                                asset.sys.id === item.fields.logo.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            return {
                                name: item.fields.name,
                                order: item.fields.order,
                                description: item.fields.description,
                                // find the linked image for the given content entry
                                src: action.payload.includes && item.fields.logo ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.logo.sys.id 
                                    ).fields.file.url : undefined
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}