export default (state = null, action) => {
    switch (action.type) {
        case "GET_PROFILES":
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.profile && action.payload.includes.Asset.find(
                            asset => 
                                asset.sys.id === item.fields.profile.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            return {
                                name: item.fields.name,
                                title: item.fields.jobTitle,
                                email: item.fields.email,
                                // find the linked image for the given content entry
                                src: action.payload.includes && item.fields.profile ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.profile.sys.id 
                                    ).fields.file.url: undefined,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                } 
            } else {
                return action.payload
            }
        default:
            return state
    }
}