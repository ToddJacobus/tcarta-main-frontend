// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_LEGAL_DOCUMENTS":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                return {
                    items:[
                        ...action.payload.items.map(item => {
                            return {
                                body: item.fields.body,
                                title: item.fields.title,
                                order: item.fields.order,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}