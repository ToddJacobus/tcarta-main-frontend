// Send content to state

export default (state = null, action) => {
    switch (action.type) {
        case "GET_CAPABILITIES_EXAMPLES":
            // Filter the payload to get only the data we need and
            // flatten it into a simple, single-level object.
            if (action.payload.total > 0) {
                const items = action.payload.items.filter(
                    item => {
                        if (item.fields.image && action.payload.includes.Asset.find(
                            asset => asset.sys.id === item.fields.image.sys.id
                        )) {
                            return true
                        } else {
                            return false
                        }
                    }
                );
                return {
                    items:[
                        ...items.map(item => {
                            return {
                                order: item.fields.order,
                                title: item.fields.title,
                                description: item.fields.description,

                                // find the linked image for the given content entry
                                url: action.payload.includes && item.fields.image ? action.payload.includes.Asset.find( 
                                        asset => asset.sys.id === item.fields.image.sys.id 
                                    ).fields.file.url : undefined,
                            }
                        }).sort((a,b) => a.order - b.order)
                    ]
                }  
            } else {
                return action.payload
            }
        default:
            return state
    }
}