import React from 'react';
import { connect } from 'react-redux';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Typography, 
    Checkbox, 
    FormControlLabel,
    Paper,
    Grid,
} from '@material-ui/core';

import Captcha from './captcha';

import {
    sendContact,
    // sendEmail,
} from '../actions';


const useStyles = makeStyles( theme => ({
    root: {
        
    },
    footerText: {
        textAlign: 'left',
        marginTop: 20,
        marginBottom: 20
    },
    phoneNumber: {
        [theme.breakpoints.down('xs')]: {
            fontSize: 12,
        }
    },
    form: {
        marginTop: 30,
        marginBottom: 30
    },
    phoneNumberContainer: {
        width: '100%',
        textAlign: 'left',
    },
    phoneNumberTable: {
        width: '100%',
    },
    checkbox: {
        marginTop: 20,
        marginBottom: 20,
    },
    button: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    buttonContainer: {
        width: '100.5',
        marginLeft: 'auto',
        marginRight: 'auto',
        textAlign: 'center',
        marginTop: 25,
        marginBotom: 5,
    },
    exploreButton: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
    },
    label: {
        marginTop: 15,
    },
    thankYouContainer: {
        width: '100%',

    },
    thankYou: {
        fontFamily: "\"Cabin\", sans-serif",
        padding: 25,
    },
}))

const ThankYou = (props) => {
    const classes = useStyles();

    return (
        <Paper className={classes.thankYouContainer}>
            <Typography
                variant="h6"
                className={classes.thankYou}
            >
                Thank you for reaching out! One of our dedicated 
                team members will get back to you soon.
            </Typography>
            
        </Paper>
    )
};

const ContactDialog = function FormDialog(props) {
    const classes = useStyles();

    const { 
        contactEmail, 
        handleDialogClose, 
        sendEmail,
        sendContact,
        handleTextDialogClickOpen, 
        open,  
        sendGaEvent,
    } = props;

    const [checked, setChecked] = React.useState(true);
    const [email, setEmail] = React.useState(contactEmail);
    const [comments, setComments] = React.useState();
    const [firstName, setFirstName] = React.useState();
    const [lastName, setLastName] = React.useState();
    const [organization, setOrganization] = React.useState();

    const [emailError, setEmailError] = React.useState(false);
    const [emailHelpText, setEmailHelpText] = React.useState();

    const [firstNameError, setFirstNameError] = React.useState(false);
    const [firstNameHelpText, setFirstNameHelpText] = React.useState();

    const [lastNameError, setLastNameError] = React.useState(false);
    const [lastNameHelpText, setLastNameHelpText] = React.useState()

    React.useEffect(() => {
        setEmail(contactEmail); 
        if (contactEmail) {
            setEmailError(!validateEmail(contactEmail))
        } else {
            setEmailError(false)
        }
    }, [contactEmail]);

    React.useEffect(() => {
        emailError ? setEmailHelpText("Please enter a valid email address") : setEmailHelpText("We'll never share your email.")
        firstNameError ? setFirstNameHelpText("Please enter your name.") : setFirstNameHelpText('')
        lastNameError ? setLastNameHelpText("Please enter your name.") : setLastNameHelpText('')

    }, [emailError, firstNameError, lastNameError]);

    const handleChecked = (event) => {
        setChecked(event.target.checked);
    };

    const validateEmail = email => {
        const valid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email) ? true : false
        return valid
    }

    const handleEmailChange = (event) => {
        let email = event.target.value
        setEmail(email)
        setEmailError(!validateEmail(email))
    };

    const handleCommentsChange = (event) => {
        setComments(event.target.value)
    };

    const handleFirstNameChange = (event) => {
        setFirstName(event.target.value)
        setFirstNameError(false)
    };

    const handleLastNameChange = (event) => {
        setLastName(event.target.value)
        setLastNameError(false)
    };

    const handleOrganizationChange = (event) => {
        setOrganization(event.target.value)
    }

    const handleFormSubmit = (captchaResponse) => {
        handleDialogClose();
        sendContact({
            email: email,
            first_name: firstName,
            last_name: lastName,
            organization: organization,
            comments: comments,
            newsletter: checked,
            ...captchaResponse,
        })

        handleTextDialogClickOpen(<ThankYou />)
    }

    return (
        <div>
            <Dialog open={open} onClose={handleDialogClose} aria-labelledby="form-dialog-title">
                <DialogContent>
                    <DialogContentText>
                        Let us know how we can meet the needs of your next project
                    </DialogContentText>
                    <form 
                        className="classes.form"
                        onSubmit={handleFormSubmit}
                    >
                      <TextField 
                            required
                            error={emailError}
                            autoFocus={email ? false : true}
                            margin="dense"
                            id="email"
                            label="Email Address"
                            helperText={emailHelpText}
                            type="email"
                            fullWidth
                            defaultValue={contactEmail}
                            value={email}
                            onChange={handleEmailChange}
                        />
                        <Grid container spacing={1} justify="space-between" >
                            <Grid item sm={6}>
                                <TextField 
                                    required
                                    error={firstNameError}
                                    autoFocus={email ? true : false}
                                    margin="dense"
                                    id="firstName"
                                    label="First Name"
                                    helperText={firstNameHelpText}
                                    type="text"
                                    value={firstName}
                                    onChange={handleFirstNameChange}
                                />
                            </Grid>
                            <Grid item sm={6}>
                                <TextField 
                                    required
                                    error={lastNameError}
                                    margin="dense"
                                    id="lastName"
                                    label="Last Name"
                                    helperText={lastNameHelpText}
                                    type="text"
                                    value={lastName}
                                    onChange={handleLastNameChange}
                                />
                            </Grid>
                        </Grid>
                        <TextField
                                    margin="dense"
                                    id="organization"
                                    label="Organization"
                                    type="text"
                                    fullWidth
                                    value={organization}
                                    onChange={handleOrganizationChange}
                                />
                        <TextField 
                            id=""
                            label="Comments or Questions?"
                            multiline
                            rows="4"
                            fullWidth
                            value={comments}
                            onChange={handleCommentsChange}
                        />  
                       <FormControlLabel
                            value="top"
                            control={<Checkbox color="primary" />}
                            label="Subscribe to our newsletter?"
                            labelPlacement="end"
                            checked={checked}
                            onChange={handleChecked}
                            className={classes.checkbox}
                        /> 
                        
                    </form>

                    <div className={classes.buttonContainer}>
                            <Captcha
                                handleFormSubmit={handleFormSubmit}

                                setEmailError={setEmailError}
                                setFirstNameError={setFirstNameError}
                                setLastNameError={setLastNameError}

                                email={email}
                                firstName={firstName}
                                lastName={lastName}
                                emailError={emailError}
                            /> 
                    </div>
                    
                    <DialogContentText className={classes.footerText}>
                        Want to talk to a real person? Give us a call.
                    </DialogContentText>
                    <div className={classes.phoneNumberContainer}>
                        <table className={classes.phoneNumberTable}>
                            <tr>
                                <th>TCarta Denver</th>
                                <th>TCarta Caribe</th>
                            </tr>
                            <tr>
                                <th>
                                    <Typography variant="subtitle1" className={classes.phoneNumber}>
                                        +1 (303) 284-6144
                                    </Typography>
                                </th>
                                <th>
                                    <Typography variant="subtitle1" className={classes.phoneNumber}>
                                        +1 (876) 817-8567
                                    </Typography>  
                                </th>
                            </tr>
                        </table>
                    </div>
                    
                    <div className={classes.buttonContainer}>
                        <Button
                            className={classes.exploreButton}
                            classes={{
                                label: classes.label,
                            }}
                            href="https://www.explore.tcarta.com"
                            target="_blank"
                            onClick={() => sendGaEvent("Explore Link", "User Click", "Contact Dialog")}
                        >
                            Explore our sample data
                        </Button>
                    </div>
                </DialogContent>
            </Dialog>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        // send_email_response: state.send_email_response,
        send_contact_response: state.send_contact_response,
    }
};

export default connect(
    mapStateToProps,
    {
        sendContact,
        // sendEmail,
    }
)(ContactDialog)