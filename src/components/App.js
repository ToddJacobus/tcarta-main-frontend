import React from 'react';
import { withStyles  } from '@material-ui/core/styles';
import { 
    CssBaseline, 
    Container, 
    Box,
} from '@material-ui/core';
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
import { loadReCaptcha } from 'react-recaptcha-google'
import ReactGA from 'react-ga';

import HeroPage from './hero_page/hero_page'
import NavBar from './navbar'
import StoriesPage from './stories_page/StoriesPage';
import ContactDialog from '../components/contactDialog';
import TextDialog from '../components/textDialog';
import Footer from '../components/footer';
import AboutPage from './about_page/aboutPage';
import NewsPage from './news_page/newsPage';
import BlogPage from './news_page/blogPage';
import PressPage from './news_page/pressPage';
import GalleryPage from './news_page/galleryPage';
import BlogDetail from './news_page/blogDetail';
import ScrollToTop from './scrollToTop';
import EventsPage from './events_page/eventsPage';
import EventDetail from './events_page/eventDetail';
import EventDetailList from './events_page/eventDetailList';
import KidsCorner from './kids_corner/kids_corner';
import ContactForm from './contact_form/contactForm';

const styles = {
    root: {
        backgroundColor: '#FCFCFC',
        overflow: 'hidden',
        
    },
    container: {
        marginTop: 80,
    },

};


class App extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            contactDialogOpen: false,
            textDialogOpen: false,
            contactEmail: null,
            navbarIndicator: 0,
            textDialogChildren: null,
            newsNavIndicator: 0,
            navBarHidden: false,
            footerHidden: false,
        };
    }
    

    componentDidMount() {
        loadReCaptcha();
    };

    sendGaEvent = (category, action, label, value) => {
        ReactGA.event({
            category,
            action,
            label,
            value,
        });
    };


    handleDialogClickOpen = () => {
        this.setState({
            contactDialogOpen: true
        })
    };

    handleDialogClose = () => {
        this.setState({
            contactDialogOpen: false
        })
    };

    handleTextDialogClickOpen = (children) => {
        this.setState({
            textDialogOpen: true,
            textDialogChildren: <div>{ children }</div>
        })
    };

    handleTextDialogClose = () => {
        this.setState({
            textDialogOpen: false
        })
    };


    handleSetContactEmail = (email) => {
        this.setState({
            contactEmail: email
        })
    }

    handleNavbarIndicatorChange = (i) => {
        this.setState({
            navbarIndicator: i
        })
    }

    handleNewsNavIndicatorChange = (i) => {
        this.setState({
            newsNavIndicator: i,
        })
    }

    setNavBarHidden = hidden => {
        this.setState({
            navBarHidden: hidden,
        })
    };

    setFooterHidden = hidden => {
        this.setState({
            footerHidden: hidden,
        })
    };


    render(){
        return (
            <Router>
                <ScrollToTop>
                    <div 
                        className={this.props.classes.root} 
                        style={this.state.appStyle}
                    >
                        <CssBaseline />
                        <div>
                            {
                                this.state.navBarHidden ? <div /> :
                                <NavBar 
                                    handleDialogClickOpen={this.handleDialogClickOpen} 
                                    handleDialogClose={this.handleDialogClose}
                                    navbarIndicator={this.state.navbarIndicator}
                                    handleNewsNavIndicatorChange={this.handleNewsNavIndicatorChange}
                                    sendGaEvent={this.sendGaEvent}
                                    {...this.props}
                                />
                            }
                        </div>
                        
                        <ContactDialog 
                                open={this.state.contactDialogOpen}
                                contactEmail={this.state.contactEmail}
                                handleDialogClose={this.handleDialogClose}
                                handleTextDialogClickOpen={this.handleTextDialogClickOpen}
                                sendGaEvent={this.sendGaEvent}
                        />
                        <TextDialog 
                                open={this.state.textDialogOpen}
                                handleTextDialogClose={this.handleTextDialogClose}
                        >
                            {this.state.textDialogChildren}
                        </TextDialog>
                        <Switch>
                            <Route path="/news/blog">
                                <NewsPage 
                                    bannerImageId="78CMSzoRcbcE0henTTBpj0" 
                                    handleNewsNavIndicatorChange={this.handleNewsNavIndicatorChange} 
                                    newsNavIndicator={this.state.newsNavIndicator}
                                >
                                    <BlogPage 
                                        newsNavIndicator={this.state.newsNavIndicator} 
                                        handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                        sendGaEvent={this.sendGaEvent}
                                    />
                                </NewsPage>
                            </Route>
                            <Route path="/news/press">
                                <NewsPage 
                                    bannerImageId="1S0UV84OUd0bgi6twL1juG" 
                                    handleNewsNavIndicatorChange={this.handleNewsNavIndicatorChange} 
                                    newsNavIndicator={this.state.newsNavIndicator} 
                                >
                                    <PressPage 
                                        newsNavIndicator={this.state.newsNavIndicator} 
                                        handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                        sendGaEvent={this.sendGaEvent}
                                    />
                                </NewsPage>
                            </Route>
                            <Route path="/news/gallery">
                                <NewsPage 
                                    bannerImageId="5T9HcGJHJgcMkzgLLpq65h" 
                                    handleNewsNavIndicatorChange={this.handleNewsNavIndicatorChange} 
                                    newsNavIndicator={this.state.newsNavIndicator}
                                >
                                    <GalleryPage 
                                        newsNavIndicator={this.state.newsNavIndicator} 
                                        handleNavbarIndicatorChange={this.handleNavbarIndicatorChange} 
                                        handleTextDialogClickOpen={this.handleTextDialogClickOpen}
                                        sendGaEvent={this.sendGaEvent}
                                    />
                                </NewsPage>
                            </Route>
                            
                            <Route 
                                path="/news/:slug" render={props => 
                                        <BlogDetail 
                                            {...props} 
                                            handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                            sendGaEvent={this.sendGaEvent}
                                        />
                                    }
                            />
                            <Route path="/news">
                                <NewsPage 
                                    bannerImageId="78CMSzoRcbcE0henTTBpj0" 
                                    handleNewsNavIndicatorChange={this.handleNewsNavIndicatorChange} 
                                    newsNavIndicator={this.state.newsNavIndicator}
                                >
                                    <BlogPage 
                                        newsNavIndicator={this.state.newsNavIndicator} 
                                        handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                        sendGaEvent={this.sendGaEvent}
                                    />
                                </NewsPage>
                            </Route>
                            <Route path="/about">
                                <AboutPage 
                                    handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                    handleDialogClickOpen={this.handleDialogClickOpen}
                                    sendGaEvent={this.sendGaEvent}
                                />
                            </Route>
                            <Route 
                                path="/events/:slug" 
                                render={
                                    props => 
                                            <EventDetailList 
                                                {...props}
                                                sendGaEvent={this.sendGaEvent}
                                                handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                            />
                                        } 
                            />
                            <Route path="/events">
                                <EventsPage 
                                    newsNavIndicator={this.state.newsNavIndicator} 
                                    handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                    sendGaEvent={this.sendGaEvent}
                                />
                            </Route>

                            <Route path="/kids">
                                <KidsCorner 
                                    sendGaEvent={this.sendGaEvent}
                                />
                            </Route>

                            <Route path="/contact" >
                                <ContactForm 
                                    handleTextDialogClickOpen={this.handleTextDialogClickOpen}
                                    sendGaEvent={this.sendGaEvent}
                                    setNavBarHidden={this.setNavBarHidden}
                                    setFooterHidden={this.setFooterHidden}
                                />
                            </Route>

                            <Route path="/">
                                <Container className={this.props.classes.container}>
                                    <Box>
                                        <HeroPage 
                                            handleDialogClickOpen={this.handleDialogClickOpen}
                                            handleSetContactEmail={this.handleSetContactEmail}
                                            handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                            sendGaEvent={this.sendGaEvent}
                                        />
                                        <StoriesPage 
                                            handleNavbarIndicatorChange={this.handleNavbarIndicatorChange}
                                            sendGaEvent={this.sendGaEvent}
                                        />
                                    </Box>
                                </Container>
                            </Route>
                        </Switch>
                        <div>
                            {
                                this.state.footerHidden ? <div /> :
                                <Footer 
                                    handleDialogClickOpen={this.handleDialogClickOpen}
                                    handleTextDialogClickOpen={this.handleTextDialogClickOpen}
                                    sendGaEvent={this.sendGaEvent}
                                />
                            }
                        </div>
                        
                    </div>  
                </ScrollToTop>
            </Router>
        )
    }
}

export default withStyles(styles)(App);

