import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import { 
    Tabs, 
    Tab, 
    Paper,
    Button,
    Menu,
    MenuItem,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import { Link, withRouter } from "react-router-dom";


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        position: 'fixed',
        overflow: 'hidden',
        top: 0,
        width: '100%',
        zIndex: 1000,
    },
    mobileHeader: {
        flexGrow: 1,
        position: 'absolute',
        overflow: 'hidden',
        top: 0,
        width: '100%',
        height: 60,
        textAlign: 'right',
        backgroundColor: 'white',
    },
    mobileHeaderLogo: {
        width: 85,
        marginRight: 20,
        marginTop: 10,
    },
    rootNarrow: {
        display: 'inline-flex',
        flexGrow: 1,
        position: 'fixed',
        overflow: 'hidden',
        bottom: 0,
        width: '100%',
        zIndex: 1000,
        marginTop: 10,
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 10,
        paddingBottom: 10,
        background: 'linear-gradient(45deg, rgba(20,20,20,0.3) 30%, rgba(150,150,150,0.5) 90%)',
        
    },
    rootWide: {
        marginTop: 10,
        marginBottom: 10,
    },
    menuButton: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        
    },
    indicator: {
        backgroundColor: "#66c24a"
    },
    logo: {
        width: 40,
        height: 40,
        padding: 5,
        marginTop: 'auto',
        marginBottom: 'auto',
    },
    contactButton: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        borderRadius: "20px 0px 20px 0px",
        marginLeft: 5,
    },
    contactButtonNarrow: {
        display: 'inline-flex',
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        borderRadius: "20px 0px 20px 0px",
        marginLeft: 'auto',
        opacity: 1,
    },
    menuItem: {
        fontSize: 22,
        fontFamily: "\"Cabin\", sans-serif",
    },
    

}))

const NavBarNarrow = props => {
    const { 
        handleDialogClickOpen,
        sendGaEvent,
    } = props;

    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleMenuClick = event => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleContactButtonClick = () => {
        sendGaEvent('Contact', 'User Click', 'Navbar Mobile')
        handleDialogClickOpen();
    };

    return (
        <div>
            <div className={classes.mobileHeader}>
                <Link to="/">
                    <img 
                        src="https://images.ctfassets.net/m7umyxqhfu1m/7si3pznQpRzqEYu1u3zExd/c18323edbfbadf7bdf15ca8b17fadd63/TCARTA_Logo_HiRes_Blue.png?h=250" 
                        alt="TCarta Logo" 
                        className={classes.mobileHeaderLogo}
                    />
                </Link>
            </div>
            <div className={classes.rootNarrow}>
                    <Button
                        aria-controls="navbar-menu" 
                        aria-haspopup="true" 
                        onClick={handleMenuClick}
                        className={classes.menuButton}
                    >
                        <MenuIcon />
                    </Button>
                    <Menu
                        id="navbar-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={Boolean(anchorEl)}
                        onClose={handleClose}
                    >
                        <MenuItem to="/" component={Link} className={classes.menuItem} onClick={handleClose} >Home</MenuItem>
                        <MenuItem to="/news" component={Link} className={classes.menuItem} onClick={handleClose} >News</MenuItem>
                        <MenuItem to="/events" component={Link} className={classes.menuItem} onClick={handleClose} >Events</MenuItem>
                        <MenuItem to="/about" component={Link} className={classes.menuItem} onClick={handleClose} >About</MenuItem>
                    </Menu>
                    <Tab 
                        onClick={handleContactButtonClick} 
                        label="Get a Quote"
                        className={classes.contactButtonNarrow}
                        classes={{
                            textColorInherit:{

                            }
                        }}
                    />
                
            </div>    
        </div>
        
        
    )
};


const NavBarWide = (props) => {
    const classes = useStyles();

    const { 
        handleDialogClickOpen, 
        navbarIndicator, 
        handleNewsNavIndicatorChange ,
        sendGaEvent,
    } = props;

    const handleNewsClick = (event) => {
        handleNewsNavIndicatorChange(0)
    }

    const handleContactButtonClick = () => {
        sendGaEvent('Contact', 'User Click', 'Navbar Desktop')
        handleDialogClickOpen();
    };

    return (
        <Paper className={classes.root} >
            <Tabs
                value={navbarIndicator}
                indicatorColor="primary"
                textColor="black"
                centered
                className={classes.rootWide}
                classes={{
                    indicator: classes.indicator,
                }}
            >
                <Tab label="Home" to="/" component={Link} />
                <Tab label="News" to="/news" component={Link} onClick={handleNewsClick} />
                <Tab label="Events" to="/events" component={Link} />
                <Tab label="About" to="/about" component={Link} />
                <Tab 
                    onClick={handleContactButtonClick} 
                    label="Get a Quote"
                    className={classes.contactButton}
                />
            </Tabs>
            
        </Paper>
    )
}

const NavBar = props => {

    const [screenWidth, setScreenWidth] = React.useState(window.innerWidth);

    const updateDimensions = event => {
        setScreenWidth(window.innerWidth);
    };

    React.useEffect(() => {
        window.addEventListener("resize", updateDimensions)
    }, [])

    return (
        <div>
            {
                screenWidth < 600 ?
                    <NavBarNarrow {...props} />
                :   <NavBarWide {...props} />
            }
        </div>
    )
}

export default withRouter(NavBar);


