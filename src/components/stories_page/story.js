import React from 'react';
import { 
    documentToReactComponents 
} from '@contentful/rich-text-react-renderer';
import { 
    makeStyles,
    Paper,
    Grid,
    Typography,
    Button,
 } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        marginBottom: 50,
        marginTop: 50,
        minHeight: "-webkit-fill-available",
        marginLeft: 'auto',
        marginRight: 'auto',
        maxWidth: 1000

    },
    darkPanel: {
        backgroundColor: '#474747',
        color: '#FAFAFA',
        height: "100vh",
        maxWidth: 500,
        position: 'relative',
    },
    lightPanel: {
        backgroundColor: '#FAFAFA',
        color: '#474747',
        height: "100vh",
        maxWidth: 500,
    },
    header: {
        position: 'relative',
        top: '10%',
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 65,
        fontFamily: "\"Raleway\", sans-serif",
        [theme.breakpoints.down('sm')]: {
            fontSize: 40,
        },
    },
    subHeader: {
        position: 'absolute',
        marginLeft: '10px',
        marginRight: '10px',
        bottom: '10%',
        fontFamily: "\"Raleway\", sans-serif",
        [theme.breakpoints.down('sm')]: {
            fontSize: 22,
        },
    },
    heroImageContainer: {
        position: 'relative',
        textAlign: 'center',
        width: "calc(100vh/2)",
        maxWidth: '100%',
        marginRight: 'auto',
        marginLeft: 'auto',
    },
    heroImage: {
        maxWidth: '100%',
        height: 'auto',
        borderRadius: 5
    },
    subPannel: {
        width: '100%',
        maxWidth: 500,
    },
    description: {
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingLeft: 10,
        fontFamily: "\"Cabin\", sans-serif",
        height: '68%',
        width: '90%',
        borderTop: '1px solid black',
        borderBottom: '1px solid black',
        fontSize: '2.25vh',
    },
    actionButton: {
        width: '100%',
        textAlign: 'center',
        bottom: 10,
        paddingTop: 60,
        paddingBottom: 20,
    },
    button: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    label: {
        marginTop: 15,
    }
}))

const Story = (props) => {
    const classes = useStyles();

    const { 
        direction, 
        header, 
        subHeader, 
        heroImage, 
        richDescription, 
        storyMapUrl,
        sendGaEvent,
    } = props;


    return (
        <div className={classes.root}>
            <Grid container 
                spacing={2}
                direction={direction}
                justify="center"
                alignItems="center"
            >
                <Grid item sm={6}>
                    <Paper elevation={0} className={classes.darkPanel}>
                        <Typography variant="h2" className={classes.header}>
                            {header}
                        </Typography>
                        <Typography variant="h4" className={classes.subHeader}>
                            {subHeader}
                        </Typography>
                    </Paper>
                </Grid>
                <Grid item sm={6}>
                    <Paper elevation={0} className={classes.lightPanel}>
                        <div className={classes.heroImageContainer}>
                            <img className={classes.heroImage} src={heroImage} alt=""/>
                        </div>
                        <div className={classes.subPannel}>
                            <Typography variant="h6" className={classes.description}>
                               {documentToReactComponents(richDescription)}
                            </Typography>
                        </div>
                        <div className={classes.actionButton} >
                                <Button 
                                    variant="contained" 
                                    className={classes.button}
                                    classes={{
                                        label: classes.label,
                                    }}
                                    href={storyMapUrl}
                                    target="_blank"
                                    onClick={() => sendGaEvent('Story Map', 'User Click', storyMapUrl)}
                                >
                                    See our ESRI Story Map
                                </Button>
                            </div>
                    </Paper>
                </Grid>
            </Grid>
        </div>
    )
}

export default Story;