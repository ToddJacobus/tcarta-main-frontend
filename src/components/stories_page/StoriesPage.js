import React from 'react';
import { 
    Button,
    makeStyles,
 } from '@material-ui/core';
import { connect } from 'react-redux';

import Story from './story'
import { fetchProductStories } from '../../actions';

const useStyles = makeStyles(theme => ({
    callToActionRoot: {
        height: 100,
        width: '82%',
        textAlign: 'center',
        borderBottom: '1px solid black',
        borderTop: '1px solid black',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    button: {
        marginTop: 20,
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    label: {
        marginTop: 15,
    },
}))

const CallToAction = (props) => {
    const classes = useStyles();

    const { sendGaEvent } = props;

    return (
        <div className={classes.callToActionRoot}>
            <Button
                className={classes.button}
                classes={{
                    label: classes.label,
                }}
                href="https://www.explore.tcarta.com"
                target="_blank"
                onClick={() => sendGaEvent('Explore Link', 'User Click', 'Stories Feed')}
            >
                Explore our sample data
            </Button>
        </div>
    )
};

const StoriesPage = (props) => {
    const { fetchProductStories, product_story_content } = props;

    React.useEffect(() => {
        fetchProductStories()
    }, []);

    return (
        <div>
            {
                product_story_content ?
                    product_story_content.items.map((item, id) =>
                        <div>
                            {id === 1 ? <CallToAction {...props} /> : <div />}
                            <Story 
                                {...item}
                                direction={id%2 ? "row-reverse": "row"}
                                {...props}
                            />
                        </div>
                    )
                : <div />
            }
        </div>
    )
};

const mapStateToProps = (state) => {
    return {
        product_story_content: state.product_story_content
    }
}

export default connect(
    mapStateToProps,
    {
        fetchProductStories,
    }
)(StoriesPage);