import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        fontFamily: "\"Cabin\", sans-serif",
    },
    gridRow: {
        flexWrap: 'wrap',
    },
    gridItem: {
        width: '100%',
        height: 'auto',
        padding: 0,
        minWidth: 260,
        margin: 20,
    },
    imageContainer: {
        height: 'auto',
        textAlign: 'center',
    },
    image: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        padding: 5,
        width: 200,
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    imageBar: {
        width: 200,
        marginLeft: 'auto',
        marginRight: 'auto',
        position: 'relative',
        bottom: 70,
        height: 70,
        backgroundColor: 'rgba(0,0,0,0.5)',
        color: 'white',
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        textAlign: 'left',
    },
    imageBarText: {
        fontFamily: "\"Cabin\", sans-serif",
    },
    nameContainer: {
        marginLeft: 5,
    },
    titleContainer: {
        marginLeft: 5,
    },
}));

const ImageBar = (props) => {
    const classes = useStyles();

    const { description } = props;

    return (
        <div className={classes.imageBar}>
            <div className={classes.nameContainer}>
                <Typography variant="subtitle2" className={classes.imageBarText} >{description}</Typography>
            </div>
        </div>
    );
};

const Image = (props) => {
    const classes = useStyles();

    const { src, description } = props;

    return (
        <Grid item sm={3} className={classes.gridItem} >
            <div className={classes.imageContainer}>
                <img src={src} className={classes.image} alt="#profilImage"/>
                { description ?  <ImageBar {...props}/> : <div></div> }
            </div>
        </Grid>
    );
};

export default (props) => {
    const classes = useStyles();

    const { images } = props;

    return (
        <div className={classes.root}>
            <Grid 
                container
                spacing={0}
                className={classes.gridRow}
                justify="center"
                alignItems="center"
                wrap
            >
                {
                    images.map((image, id) => {
                        return <Image {...image} key={id} />
                    })
                }
            </Grid>
            
        </div>
    )
};