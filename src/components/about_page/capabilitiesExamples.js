import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Grid, Typography
} from '@material-ui/core';


const ITEM_WIDTH = 350;

const useStyles = makeStyles( theme => ({
    root: {
        maxWidth: 800,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    gridItem: {
    },
    titleContainer: {
        width: ITEM_WIDTH,
        marginTop: 20,
        marginBottom: 10,
        marginLeft: 'auto',
        marginRight: 'auto',
        textAlign: 'left',
    },
    title: {
        fontFamily: "\"Cabin\", sans-serif",
    },
    imageContainer: {
        width: ITEM_WIDTH,
        height: ITEM_WIDTH,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    image: {
        width: '100%',
        height: 'auto',
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    imageBar: {
        width: ITEM_WIDTH,
        marginLeft: 'auto',
        marginRight: 'auto',
        position: 'relative',
        bottom: 65,
        height: 65,
        backgroundColor: 'rgba(0,0,0,0.5)',
        color: 'white',
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        textAlign: 'left',
    },
    descriptionContainer: {
        marginLeft: 5,
    },
    description: {
        fontFamily: "\"Cabin\", sans-serif",
    },
}));

const Item = (props) => {
    const classes = useStyles();

    const { title, url, description } = props;

    return (
        <Grid item sm={6} className={classes.gridItem}>
            <div className={classes.titleContainer}>
                <Typography variant="h6" className={classes.title} >{title}</Typography>
            </div>
            <div className={classes.imageContainer}>
                <img src={url} alt={title} className={classes.image} />
            </div>
            <div className={classes.imageBar}>
                <div className={classes.descriptionContainer}>
                    <Typography variant="subtitle1" className={classes.description} >{description}</Typography>
                </div>
            </div>
        </Grid>
    )
};

export default (props) => {
    const classes = useStyles();

    const { items } = props;

    return (
        <Grid 
            container 
            spacing={0} 
            alignItems="center" 
            justify="center" 
            className={classes.root}
        >
            {
                items.map((item, id) => {
                    return <Item {...item} key={id}/>
                })
            }
        </Grid>
    )
};