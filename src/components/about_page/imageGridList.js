import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {
    GridList, 
    GridListTile,
    GridListTileBar,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        overflow: 'hidden',
        backgroundColor: theme.palette.background.paper,
    },
    gridList: {
        flexWrap: 'nowrap',
        transform: 'translateZ(0)'
    },
    tile: {
        width: 300,
        height: 300,
    },
    title: {
        color: theme.palette.primary.light,
    },
    titleBar: {
        background: 'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
}))


export default (props) => {
    const classes = useStyles();

    const { images } = props;

    return (
       <div className={classes.root}>
           <GridList 
            className={classes.gridList} 
            cols={3}
            cellHeight={300}
            >
                {
                    images.map((tile, id) => {
                        return (
                            <GridListTile 
                                key={id}
                                cols={1} 
                                rows={1}
                                className={classes.tile}
                            >
                                <img src={tile.src} alt={tile.name} />
                                <GridListTileBar 
                                    title={tile.name}
                                    classes={{
                                        root: classes.titleBar,
                                        title: classes.title,
                                    }}
                                />
                            </GridListTile>
                        )
                        
                    })
                }
           </GridList>
       </div>
    )
};