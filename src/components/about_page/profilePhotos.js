import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Grid, Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    root: {
        fontFamily: "\"Cabin\", sans-serif",
        flexWrap: 'wrap',
        overflow: 'auto',
    },
    gridRow: {
        flexWrap: 'nowrap',
    },
    gridItem: {
        width: '100%',
        height: 'auto',
        padding: 0,
        minWidth: 260,
    },
    imageContainer: {
        height: 'auto',
        textAlign: 'center',
    },
    image: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: 250,
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    imageBar: {
        width: 250,
        marginLeft: 'auto',
        marginRight: 'auto',
        position: 'relative',
        bottom: 65,
        height: 65,
        backgroundColor: 'rgba(0,0,0,0.5)',
        color: 'white',
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        textAlign: 'left',
    },
    imageBarText: {
        fontFamily: "\"Cabin\", sans-serif",
    },
    nameContainer: {
        marginLeft: 5,
    },
    titleContainer: {
        marginLeft: 5,
    },
}));

const ImageBar = (props) => {
    const classes = useStyles();

    const { name, title } = props;

    return (
        <div className={classes.imageBar}>
            <div className={classes.nameContainer}>
                <Typography variant="subtitle1" className={classes.imageBarText} >{name}</Typography>
            </div>
            <div className={classes.titleContainer}>
                <Typography variant="caption" className={classes.imageBarText} >{title}</Typography>  
            </div>
        </div>
    );
};

const GridItem = (props) => {
    const classes = useStyles();

    const { src, title } = props;

    return (
        <Grid item sm={6} className={classes.gridItem} >
            <div className={classes.imageContainer}>
                <img src={src} className={classes.image} alt="#profilImage"/>
                { title ?  <ImageBar {...props}/> : <div></div> }
            </div>
        </Grid>
    );
};

export default (props) => {
    const classes = useStyles();

    const { images } = props;

    return (
        <div className={classes.root}>
            <Grid 
                container
                spacing={0}
                className={classes.gridRow}
                justify="flex-start"
                alignItems="center"
                wrap
            >
                {
                    images.filter((image, i ) => i % 2 ? false: true).map((image, id) => {
                        return <GridItem {...image} id={id} key={id} />
                    })
                }
            </Grid>
            <Grid 
                container
                spacing={0}
                className={classes.gridRow}
                justify="flex-start"
                alignItems="center"
                wrap
            >
                {
                    images.filter((image, i) => i % 2 ? true: false).map((image, id) => {
                        return <GridItem {...image} id={id} key={id} />
                    })
                }
            </Grid>
            
        </div>
    )
};