import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Typography, 
    Paper,
    Grid,
    Button,
} from '@material-ui/core';


const useStyles = makeStyles( theme => ({
    root: {
        marginTop: 120,
        marginBottom: 100,
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingTop: 20,
        maxWidth: 1000,

    },
    borderContainer: {
        width: '85%',
        marginLeft: 'auto',
        paddingBottom: 100,
        borderTop: '1px solid black'
    },
    headerContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '60%',
        textAlign: 'left',
    },
    header: {
        width: '100%',
        padding: 10,
        fontFamily: "\"Raleway\", sans-serif",
    },
    textBodyContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '60%',
        textAlign: 'justify',
    },
    textBody: {
        width: '100%',
        marginLeft: 40,
        padding: 10,
        fontFamily: "\"Cabin\", sans-serif",
    },
    contactButtonContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 50,
        padding: 20,
        width: 350,
        textAlign: 'center',
    },
    contactButton: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    buttonHeader: {
        textAlign: 'justify',
        marginBottom: 20,
        fontFamily: "\"Cabin\", sans-serif",
    },
    logoTitle: {
        fontFamily: "\"Cabin\", sans-serif",
        padding: 20,

    },
    logoContainer: {
        width: '100%',
        textAlign: 'center',
    },
    logo: {
        maxWidth: 260,
        height: 'auto',
    },
}));

export default (props) => {
    const classes = useStyles();

    const { 
        handleDialogClickOpen,
        sendGaEvent,
    } = props;

    return (
        <div>
            <Grid container spacing={2} justify="center" alignItems="flex-start" className={classes.root}>
                <Grid item sm={6}>
                    <div className={classes.headerContainer}>
                        <Typography variant="h4" className={classes.header} >Our Work Supports Global Initiatives</Typography>
                    </div>
                    <div className={classes.textBodyContainer}>
                        <div className={classes.logoContainer}>
                            <img className={classes.logo} src="https://images.ctfassets.net/m7umyxqhfu1m/5MhQ0EicLgqHDMyLqVm5Nb/77174b6a8ef5c781ea7e41615a7c1578/combined-logos.png?h=250" alt="combined seabed 2020 logos"/>
                            <Typography className={classes.logoTitle} variant="h6">Seabed 2020</Typography>
                        </div>
                        
                        <div className={classes.logoContainer}>
                            <img className={classes.logo} src="https://images.ctfassets.net/m7umyxqhfu1m/39PAoW7h0dRFRe9nFQSINb/2373b13d0ccc8ea301298aefa95631a3/UN.png?h=250" alt="United Nations Logo"/>
                            <Typography className={classes.logoTitle} variant="h6">UN Sustainability Goals</Typography>
                        </div>
                    </div>
                </Grid>
                <Grid item sm={6}>
                    <div className={classes.headerContainer}>
                        <Typography variant="h4" className={classes.header} >Our Clients</Typography>
                    </div>
                    <div className={classes.textBodyContainer}>
                        <ul>
                            <li>U.S. and International Governments</li>
                            <li>Non-Governmental Organizations (NGO)</li>
                            <li>International Engineering Firms</li>
                            <li>Major Oil and Gas</li>
                            <li>Geophysics and Hydrographic Survey Companies</li>
                            <li>Scientific and Research Organizations</li>
                            <li>Many, many more...</li>
                        </ul>
                    </div>
                </Grid>
            </Grid>
            <Paper className={classes.contactButtonContainer}>
                <Typography variant="h6" className={classes.buttonHeader} >Contact us for our past performance and project history</Typography>
                <Button 
                    className={classes.contactButton} 
                    variant="contained" 
                    onClick={() => {
                        sendGaEvent("Contact", "User Click", "Past Performance");
                        handleDialogClickOpen();
                    }}
                >
                    get in touch
                </Button>
            </Paper>
        </div>
    );
};