import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Grid, Typography
} from '@material-ui/core';

const useStyles = makeStyles( theme => ({
    root: {},
    gridItem: {
        marginTop: 30,
        marginBottom: 30,
        marginLeft: 'auto',
        marginRight: 'auto',
        maxWidth: 800,
    },
    actionPhotoContainer: {
        width: 300,
        height: 300,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    actionPhoto: {
        width: '100%',
        height: 'auto',
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    wordCloudContainer: {
        width: 300,
        height: 300,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 20,
        marginBottom: 20,
    },
    wordCloud: {
        width: '100%',
        height: 'auto',
    },
    descriptionContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '95%',
        textAlign: 'center',
        color: '#cecece',

    },
    description: {
        fontFamily: "\"Cabin\", sans-serif",
    },
}));


const Item = (props) => {
    const classes = useStyles();

    const { id, description, actionPhotoUrl, wordCloudUrl } = props;

    return (
        <div>
            <div className={classes.descriptionContainer} >
                <Typography variant="h6" className={classes.description}>{description}</Typography>
            </div>
            <Grid 
                container
                alignItems="center"
                justify="center"
                direction={id%2 ? "row-reverse" : "row"}
                className={classes.gridItem}
            >
                <Grid item sm={6}>
                    <div className={classes.actionPhotoContainer} >
                        <img src={actionPhotoUrl} alt="TCarta Action" className={classes.actionPhoto} />
                    </div>
                </Grid>
                <Grid item sm={6} >
                    <div className={classes.wordCloudContainer} >
                        <img src={wordCloudUrl} alt={description} className={classes.wordCloud} />
                    </div>
                </Grid>
            </Grid>    
        </div>
        
    )
};


export default (props) => {
    const classes = useStyles();

    const { items } = props;

    return (
        <div
            className={classes.root}
        >
            {
                items.map((item, id) => {
                    return <Item {...item} id={id} key={id} />
                })
            }
        </div>
    )
};