import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Typography, 
    Paper,
    Button,
} from '@material-ui/core';

import ReactGA from 'react-ga';

import ProfilePhotos from './profilePhotos';
import LogoGrid from './logoGrid';
import CapabilitiesExamples from './capabilitiesExamples';
import WordClouds from './wordClouds';
import TextBox from './textBox';

import { 
    fetchProfilePhotos, 
    fetchPartnershipLogos, 
    fetchCapabilitiesGif,
    fetchCapabilitiesExamples,
    fetchWordClouds,
    fetchGrantOrgs,
} from '../../actions';

import { connect } from 'react-redux';

const useStyles = makeStyles( theme => ({
    root: {
        marginLeft: 'auto',
        marginRight: 'auto',
        maxWidth: 1200,
    },
    headerContainer: {
        paddingTop: 50,
        paddingBottom: 50,
        width: "100%",
        maxWidth: 1200,
        marginRight: 'auto',
        marginLeft: 'auto',
        textAlign: "right",
    },
    header: {
       position: 'relative',
       top: '40%',
       marginLeft: "20%",
       marginRight: 10,
       marginTop: 20,
       marginBottom: 50,
       fontFamily: "\"Raleway\", sans-serif",
    },
    headerBorder: {
        width: '85%',
        borderTop: '1px solid black',
        marginLeft: 'auto',
        marginTop: 100,
    },
    subHeaderContainer: {
        paddingTop: 50,
        paddingBottom: 50,
        width: '100%',
        maxWidth: 1200,
        marginRight: 'auto',
        marginLeft: 'auto',
        textAlign: 'right',
    },
    subHeader: {
        position: 'relative',
       top: '40%',
       marginLeft: "20%",
       marginRight: 10,
       marginTop: 100,
       marginBottom: 50,
       fontFamily: "\"Raleway\", sans-serif",
    },
    capabilitiesContainer: {
        width: '100%',
        maxWidth: 800,
        textAlign: 'center',
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingLeft: 10,
        paddingRight: 10,
    
    },
    capabilities: {
        width: '100%',
        height: 'auto',
        padding: 5,
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    capabilitiesExamplesContainer: {},
    imageGridBasicContainer: {
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: 10,
        marginBottom: 20,
        maxWidth: 1073,
    },
    recruitmentContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: 350,
        textAlign: 'justify',
        marginBottom: 50,
        paddingBottom: 50,
    },
    recruitmentPaper: {},
    recruitment: {
        fontFamily: "\"Cabin\", sans-serif",
        padding: 20,
    },
    contactButtonContainer: {
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 50,
        padding: 20,
        width: 350,
        textAlign: 'center',
    },
    contactButton: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    buttonHeader: {
        textAlign: 'justify',
        marginBottom: 20,
        fontFamily: "\"Cabin\", sans-serif",
    },

}))

const Heading = (props) => {
    const classes = useStyles();

    const { text } = props;

    return (
        <div className={classes.headerContainer}>
            <div className={classes.headerBorder}></div>
            <Typography className={classes.header} variant="h4">{text}</Typography>
        </div>
    )
}


const AboutPage = (props) => {
    const classes = useStyles();

    const { 
        fetchProfilePhotos,
        fetchPartnershipLogos,
        fetchCapabilitiesGif,
        fetchCapabilitiesExamples,
        fetchWordClouds,
        fetchGrantOrgs,

        sendGaEvent,

        capabilities_gif_content,
        capabilities_examples_content,
        word_cloud_content,
        profile_photos_content,
        grant_orgs_content,
        partnership_logos_content,

        handleNavbarIndicatorChange,
        handleDialogClickOpen,
     } = props;

    React.useEffect(() => {
        fetchProfilePhotos();
        fetchPartnershipLogos();
        fetchCapabilitiesGif();
        fetchCapabilitiesExamples();
        fetchWordClouds();
        fetchGrantOrgs();

        handleNavbarIndicatorChange(3);
    }, [])

    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    return (
        <div>
            <Paper className={classes.root}>

                <Heading text={"Our Core Capabilities"} />
                <div className={classes.capabilitiesContainer}>
                    <img className={classes.capabilities} src={
                        capabilities_gif_content && 
                        capabilities_gif_content.items.length > 0 ? 
                        capabilities_gif_content.items[0].url : "#"
                        } alt="" /> 
                </div>

                <Heading text={"Our Expertise Applied"} />
                <div className={classes.capabilitiesExamplesContainer}>
                    <CapabilitiesExamples items={capabilities_examples_content ? capabilities_examples_content.items : []} />
                </div>

                <Heading text={"Our Core Values"} />
                <div className={classes.wordCloudsContainer} >
                    <WordClouds items={word_cloud_content ? word_cloud_content.items : []} />
                </div>

                <Heading text={"Who we Are"}/>
                <div className={classes.imageGridListContainer}>
                    <ProfilePhotos images={profile_photos_content ? profile_photos_content.items : []}/>
                </div>

                <Heading text={"Our Research Grants"} />
                <div className={classes.imageGridBasicContainer}>
                    <LogoGrid images={grant_orgs_content ? grant_orgs_content.items : []}/>
                    
                    <Paper className={classes.contactButtonContainer}>
                        <Typography variant="h6" className={classes.buttonHeader} >Contact us to find out more about our research grants</Typography>
                        <Button 
                            className={classes.contactButton} 
                            variant="contained" 
                            onClick={() => {
                                sendGaEvent("Contact", "User Click", "Research Grants");
                                handleDialogClickOpen();
                            }}
                        >
                            get in touch
                        </Button>
                    </Paper>
                    
                </div>

                <Heading text={"Our Partnerships and Affiliations"} />
                <div className={classes.imageGridBasicContainer}>
                    <LogoGrid images={partnership_logos_content ? partnership_logos_content.items : []}/>
                </div>

                <TextBox {...props}/>

                <Heading text={"Join Us"} />
                <div className={classes.recruitmentContainer}>
                    <Paper className={classes.recruitmentPaper}>
                        <Typography 
                            variant="subtitle1" 
                            className={classes.recruitment}
                        >
                             We have no open positions, but welcome
                             talented, motivated individuals to
                             submit materials for review.  In the meanwhile,
                             please follow us on social media.
                        </Typography>
                    </Paper>
                    
                </div>
                
            </Paper>
        </div>
    )  
};

const mapStateToProps = (state) => {
    return {
        profile_photos_content: state.profile_photos_content,
        partnership_logos_content: state.partnership_logos_content,
        capabilities_gif_content: state.capabilities_gif_content,
        capabilities_examples_content: state.capabilities_examples_content,
        word_cloud_content: state.word_cloud_content,
        grant_orgs_content: state.grant_orgs_content,
    }
}

export default connect(
    mapStateToProps,
    {
        fetchProfilePhotos,
        fetchPartnershipLogos,
        fetchCapabilitiesGif,
        fetchCapabilitiesExamples,
        fetchWordClouds,
        fetchGrantOrgs,
    }
)(AboutPage);