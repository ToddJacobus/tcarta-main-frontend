import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Link, Grid } from '@material-ui/core';
import { Link as RouterLink } from "react-router-dom";

const useStyles = makeStyles(theme => ({
    root: {
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '90%',
        maxWidth: 1200,
    },
    postRoot: {
        width: '100%',
        maxWidth: 1200,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 20,
        marginBottom: 125,
        borderTop: '1px solid #474747',
        color: '#474747',
        textAlign: 'center',
    },
    image: {
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        width: '100%',
        maxWidth: 300,
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    imageContainer: {
        marginTop: 45,
    },
    imageBar: {
        width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
        position: 'relative',
        bottom: 100,
        height: 100,
        backgroundColor: 'rgba(0,0,0,0.5)',
        color: 'white',
        borderBottomRightRadius: 5,
        borderBottomLeftRadius: 5,
        textAlign: 'left',
        
    },
    title: {
        marginTop: '10%',
        marginBottom: '10%',
        fontFamily: "\"Cabin\", sans-serif",
    },
    nameContainer: {
        marginLeft: 5,
    },
    postCaptionContainer: {
        textAlign: 'left',

    },
    date: {
        fontFamily: "\"Cabin\", sans-serif",
    },
    description: {
        marginTop: 20,
        marginBottom: 20,
        fontFamily: "\"Cabin\", sans-serif",
    },
    readMorebuttonContainer: {
        width: '100%',
        marginTop: '20%',
    },
    readMoreButton: {
        color: '#474747',
        fontFamily: "\"Cabin\", sans-serif",
        marginBottom: 30,
    },
    buttonContainer: {
        width: '100%',
        textAlign: 'center'
    },
    button: {},
}));


const PostContent = (props) => {
    const classes = useStyles();

    const {
        url, 
        title, 
        publishDate, 
        description,
        slug,
    } = props;

    return (
            <Grid container spacing={1} justify="space-around" alignItems="flex-start" >
                <Grid item sm={6} className={classes.imageContainer}>
                    <img src={url} className={classes.image} alt="#profilImage"/>
                </Grid>
                <Grid item sm={6}>
                    <div className={classes.postCaptionContainer}>
                        <Typography variant="h6" className={classes.title} >{title}</Typography>
                        <div>
                            <Typography variant="caption" className={classes.date}>{publishDate}</Typography>
                            <Typography variant="subtitle1" className={classes.description} >{description}</Typography> 
                        </div>    
                    </div>
                    
                    <div className={classes.readMorebuttonContainer}>
                        <Link
                            component={RouterLink}
                            variant="h6"
                            color="white"
                            className={classes.readMoreButton}
                            to={`/news/${slug}`}
                        >
                            Read More
                        </Link>
                    </div>
                </Grid>
            </Grid>
    );
};

const Post = (props) => {
    const classes = useStyles();

    return (
        <div className={classes.postRoot}>
            <PostContent {...props} />
        </div>
    )
};

export default (props) => {
    const { items } = props;

    const classes = useStyles();

    return (
        <div className={classes.root} >
            {
                items.map((post, id) => {
                    return (
                        <Post {...post} key={id} />
                    )
                })
            }
            
        </div>
    )
}