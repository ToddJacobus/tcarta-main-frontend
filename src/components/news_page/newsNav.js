import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Tabs, Tab } from '@material-ui/core';

import { Link, withRouter } from "react-router-dom";

const useStyles = makeStyles(theme => ({
    newsNavRoot: {
        top: 70,
        left: 3,
        position: 'fixed',
        zIndex: 999,
        [theme.breakpoints.down('xs')]: {
            top: 3,
        },
    },
    indicator: {
        backgroundColor: "#00a3e0"
    },
}))

const NewsNav = (props) => {
    const classes = useStyles();

    const { newsNavIndicator, handleNewsNavIndicatorChange } = props;

    React.useEffect(() => {
        const pathObject = {
            '/news': 0,
            '/news/blog': 0,
            '/news/press':1,
            '/news/gallery':2
        }
        if (pathObject[props.history.location.pathname]) {
            handleNewsNavIndicatorChange(pathObject[props.history.location.pathname]);
        };
        
    }, [])

    const handleBlogClick = () => {
        handleNewsNavIndicatorChange(0);
    };

    const handlePressClick = () => {
        handleNewsNavIndicatorChange(1);
    };

    const handleGalleryClick = () => {
        handleNewsNavIndicatorChange(2);
    };

    const handleEventsClick = () => {
        handleNewsNavIndicatorChange(3);
    };

    return (
        <Paper className={classes.newsNavRoot} >
            <Tabs
                value={newsNavIndicator}
                indicatorColor="primary"
                textColor="black"
                centered
                classes={{
                    indicator: classes.indicator
                }}
            >
                <Tab label="Blog" to="/news/blog" component={Link} onClick={handleBlogClick}/>
                <Tab label="Press" to="/news/press" component={Link} onClick={handlePressClick}/>
                <Tab label="Gallery" to="/news/gallery" component={Link} onClick={handleGalleryClick}/>
                
            </Tabs>
        </Paper>
    )
};

export default withRouter(NewsNav);