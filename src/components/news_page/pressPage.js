import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Button } from '@material-ui/core';

import ReactGA from 'react-ga';

import BlogFeed from './blogFeed';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 60,
    },
    feedFooter: {
        width: '96%',
        height: 5,
        borderBottom: '1px solid black',
        marginRight: 'auto',
        marginLeft: 'auto',

    },
    pressReleaseContainer: {},
    headerContainer: {
        width: "80%",
        textAlign: 'left',
        marginTop: 10,
        marginBottom: 45,
        marginLeft: 20,
        
    },
    primaryFont: {
        fontFamily: "\"Raleway\", sans-serif",
    },
    buttonContainer: {
        width: '100%',
        textAlign: 'center',
        marginBottom: 20,
    },
    button: {
        color: '#66c24A'
    },
}))

export default (props) => {
    const classes = useStyles();

    const { 
        handleNavbarIndicatorChange, 
        handleLoadMorePress, 
        pressReleases, 
        hasMorePress,
        blog_banner_content,
        sendGaEvent,
    } = props;

    React.useEffect(() => {
        handleNavbarIndicatorChange(1)
    }, []);

    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    return (
        <div>
            <div className={classes.headerContainer}>
                <Typography variant="h3" className={classes.primaryFont} >{blog_banner_content ? blog_banner_content.items.title : ""}</Typography>
            </div>
            <BlogFeed {...props} items={pressReleases ? pressReleases : []}/>
            { hasMorePress ?
                <div className={classes.buttonContainer}>
                    <Button
                        onClick={() => {
                            sendGaEvent('Load More Button', 'User Click' );
                            handleLoadMorePress();
                        }}
                        color="primary"
                        className={classes.button}
                    >
                        Load More
                    </Button>
                </div> :
                <div></div>
            }
            <div className={classes.feedFooter}></div>
        </div>
    );
};