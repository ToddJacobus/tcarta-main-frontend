import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';



import { 
    fetchBlogPosts, 
    fetchPressReleases,
    fetchBlogBannerImage,
    fetchGalleryImages,
    fetchEvents,
 } from '../../actions';

import NewsNav from './newsNav';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 60,
        maxWidth: 1200,
        width: '100%',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    newsNavRoot: {
        top: 60,
        left: 10,
        position: 'fixed',
    },
    blogContainer: {
        height: '100vh',
        margin: 10,
    },
    feedFooter: {
        width: '96%',
        height: 5,
        borderBottom: '1px solid black',
        marginRight: 'auto',
        marginLeft: 'auto',

    },
    instagramContainer: {
        border: '1px solid black',
        height: '100vh',
    },
    primaryFont: {
        fontFamily: "\"Raleway\", sans-serif",
    },
    indicator: {
        backgroundColor: "#00a3e0"
    },
    bannerImageContainer: {
        width: '100%',
        maxWidth: 1200,
        marginLeft: 'auto',
        marginRight: 'auto',
        textAlign: 'center',
    },
    bannerImage: {
        width: '100%',
        height: 'auto',
        borderRadius: 5,
    },

}))


const NewsPage = (props) => {
    const classes = useStyles();

    const { 
        fetchBlogPosts, 
        blog_posts_content, 
        fetchPressReleases, 
        press_releases_content,
        fetchBlogBannerImage,
        blog_banner_content,
        fetchGalleryImages,
        bannerImageId,

        children,
    } = props;

    // Number of posts loaded before the Load More button renders
    const LIMIT = 30;

    // State and handlers for Blog Posts
    const [total, setTotal] = React.useState(0)
    const [skip, setSkip] = React.useState(0);
    const [pageNumber, setPageNumber] = React.useState(1)
    const [hasMore, setHasMore] = React.useState(true);
    const [posts, setPosts] = React.useState([]);

    // State and handlers for Press Releases
    const [totalPress, setTotalPress] = React.useState(0);
    const [skipPress, setSkipPress] = React.useState(0);
    const [pageNumberPress, setPageNumberPress] = React.useState(1);
    const [hasMorePress, setHasMorePress] = React.useState(true);
    const [pressReleases, setPressReleases] = React.useState([]);

    // Use state to keep track of previously loaded items
    const handlesetPosts = () => {
        setPosts(oldItems => [...oldItems, ...blog_posts_content.items])
    };

    const handleSetPressReleases = () => {
        setPressReleases(oldItems => [...oldItems, ...press_releases_content.items])
    };


    const handleLoadMore = () => {
        if (hasMore) {
            setSkip( pageNumber*LIMIT )
            setPageNumber(pageNumber + 1)  
        }
        setHasMore(skip <= total)
    }

    const handleLoadMorePress = () => {
        if (hasMorePress) {
            setSkipPress( pageNumberPress*LIMIT)
            setPageNumberPress(pageNumberPress + 1)
        }
        setHasMorePress(skipPress <= totalPress)
    }


    React.useEffect(() => {
        if (hasMore) {
            fetchBlogPosts({ limit: LIMIT, skip: skip });
        }
    }, [skip, hasMore])

    React.useEffect(() => {
        if (hasMorePress) {
            fetchPressReleases({limit: LIMIT, skip: skipPress});
        }
    }, [skipPress, hasMorePress]);


    React.useEffect(() => {
        setTotal(blog_posts_content ? blog_posts_content.total : 0)
        
        if (blog_posts_content) {
            handlesetPosts(blog_posts_content.items)
            if (blog_posts_content.total <= LIMIT) {
                setHasMore(false)
            }
        }

    }, [blog_posts_content]);

    React.useEffect(() => {
        setTotalPress(press_releases_content ? press_releases_content.total : 0)

        if (press_releases_content) {
            handleSetPressReleases(press_releases_content.items)
            if (press_releases_content.total <= LIMIT) {
                setHasMorePress(false)
            }
        }

    }, [press_releases_content])

    React.useEffect(() => {
        fetchBlogBannerImage({entryId: bannerImageId});
    }, [fetchBlogBannerImage, bannerImageId]);

    React.useEffect(() => {
        fetchGalleryImages();

        // reset the state and skip values when component mounts.
        setPressReleases([]);
        setPosts([]);
        setSkip(0);
        setSkipPress(0);

        setHasMore(skip <= total)
        setHasMorePress(skipPress <= totalPress)

    }, []);

    return (
        <div className={classes.root}>
            <NewsNav {...props} />
            <div className={classes.bannerImageContainer} >
                <img src={blog_banner_content ? blog_banner_content.items.url : "#"} className={classes.bannerImage} alt=""/>
            </div>
            { React.cloneElement(children, { ...props, handleLoadMore, handleLoadMorePress, hasMore, hasMorePress, posts, pressReleases }) }
        </div>
    )
};


const mapStateToProps = (state) => {
    return {
        blog_posts_content: state.blog_posts_content,
        press_releases_content: state.press_releases_content,
        blog_banner_content: state.blog_banner_content,
        gallery_images_content: state.gallery_images_content,
        events_content: state.events_content,
    }
};

export default connect(
    mapStateToProps,
    {
        fetchBlogPosts,
        fetchPressReleases,
        fetchBlogBannerImage,
        fetchGalleryImages,
        fetchEvents,
    }
)(NewsPage);