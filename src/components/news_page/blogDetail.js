import React from 'react';
import { connect } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Typography, 
    Paper, 
    Grid,
    Button,
} from '@material-ui/core';

import ReactGA from 'react-ga';

import RenderContentfulRichText from './rtfRenderer';
import { fetchBlogPosts, fetchPressReleases } from '../../actions';


const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 60,
    },
    postRoot: {
        width: "95%",
        maxWidth: 800,
        marginLeft: 'auto',
        marginRight: 'auto',
        padding: 40,

    },
    bannerContainer: {
        borderBottom: '1px solid black',
        marginBottom: 20,
        paddingBottom: 20,
    },
    titleContainer: {},
    title: {
        marginTop: 10,
        fontFamily: "\"Raleway\", sans-serif",
    },
    publishDate: {
        marginTop: 20,
        fontFamily: "\"Cabin\", sans-serif",
    },
    author: {
        fontFamily: "\"Cabin\", sans-serif",
        marginTop: 50,
        marginBottom: 50,
    },
    imageContainer: {
        width: '100%',
        textAlign: 'center',
    },
    image: {
        width: 300,
        borderRadius: 5,
        boxShadow: '0 4px 20px 0 rgba(0, 0, 0, 0.2), 0 6px 30px 0 rgba(0, 0, 0, 0.19)',
    },
    contentContainer: {
        fontFamily: "\"Cabin\", sans-serif",
    },
    callToActionContainer: {
        width: '100%',
        height: 200,
        position: 'relative',
        marginTop: 10,
        marginBottom: 10,
    },
    actionTitleContainer: {
        width: '100%',
        position: 'absolute',
        top: '10%',
        textAlign: 'center',
    },
    actionTitle: {
        fontFamily: "\"Cabin\", sans-serif",
        fontSize: 24,
    },
    actionButtonContainer: {
        width: '100%',
        textAlign: 'center',
        position: 'absolute',
        bottom: '15%',
    },
    actionButton: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        paddingTop: 15,
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4,
    },
}));

const Post = (props) => {
    const classes = useStyles();

    const {
        item, 
        blog_posts_content, 
        press_releases_content,
        sendGaEvent,
    } = props;

    const handleLinkedAssets = () => {
        const blogPosts = blog_posts_content.linkedAssets ? blog_posts_content.linkedAssets : []
        const pressReleases = press_releases_content.linkedAssets ? press_releases_content.linkedAssets : []

        return [ ...blogPosts, ...pressReleases]
    };

    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    return (
        <div>
            <Paper className={classes.postRoot}>
                <Grid container spacing={1} justify="space-around" className={classes.bannerContainer}>
                    <Grid item sm={6}>
                        <div className={classes.titleContainer}>
                            <Typography variant="h4" className={classes.title} >{item.title}</Typography>
                            <Typography variant="subtitle1" className={classes.publishDate} >{item.publishDate}</Typography>
                            <Typography variant="h6" className={classes.author} >{item.author}{item.authorTitle ? `, ${item.authorTitle}` : ""}</Typography>
                        </div>
                    </Grid>
                    <Grid item sm={6}>
                        <div className={classes.imageContainer}>
                            <img src={item.url} alt={item.title} className={classes.image}/>
                        </div>
                    </Grid>
                </Grid>
                {   item.actionTitle && item.actionLink ?
                    <div className={classes.callToActionContainer}>
                        <div className={classes.actionTitleContainer}>
                            <Typography className={classes.actionTitle}>{item.actionTitle}</Typography>
                        </div>
                        <div className={classes.actionButtonContainer}>
                            <Button className={classes.actionButton} href={item.actionLink}>
                                Go to Event
                            </Button>
                        </div>
                    </div>
                    : <div />
                }
                
                <div className={classes.contentContainer} >
                   { RenderContentfulRichText(
                            item.body, 
                            handleLinkedAssets(props),
                            sendGaEvent,
                        ) 
                    }
                </div>
                
            </Paper>
        </div>
        
    );
};


const BlogDetail = (props) => {
    const classes = useStyles();

    const {
        match,
        handleNavbarIndicatorChange,
        fetchBlogPosts, 
        fetchPressReleases, 
        blog_posts_content, 
        press_releases_content
    } = props;


    React.useEffect(() => {
        fetchBlogPosts({slug: match.params.slug});
        fetchPressReleases({slug: match.params.slug});
        handleNavbarIndicatorChange(1);
    }, [match.params.slug])

    
    return (
        <div className={classes.root}>
            { 
                blog_posts_content && 
                press_releases_content ?
                    <Post 
                        {...props}
                        item={
                            [
                                ...blog_posts_content.items, 
                                ...press_releases_content.items
                            ].find(item => item.slug === match.params.slug)}/>    : 
                    <div></div> 
            }
        </div>
    )
};


const mapStateToProps = (state) => {
    return {
        blog_posts_content: state.blog_posts_content,
        press_releases_content: state.press_releases_content,
    }
};

export default connect (
    mapStateToProps,
    {
        fetchBlogPosts,
        fetchPressReleases,
    }
)(BlogDetail)