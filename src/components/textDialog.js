import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import { Button } from '@material-ui/core';


export default (props) => {

    const { open, handleTextDialogClose, children } = props;

    return (
        <div>
            <Dialog 
                open={open} 
                onClose={handleTextDialogClose} 
                aria-labelledby="form-dialog-title" 
                maxWidth="md"
                fullWidth={true}
            >
                <DialogContent>
                    { children }
                </DialogContent>

            <DialogActions>
                <Button
                    onClick={handleTextDialogClose}
                >
                    Close
                </Button>
            </DialogActions>
            </Dialog>
        </div>
    )
};