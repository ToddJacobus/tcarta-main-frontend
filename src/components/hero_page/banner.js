import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    root: {
        maxWidth: 600
    },
    button: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    textField: {
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4,
        fontFamily: "\"Cabin\", sans-serif"

    },
    header: {
        fontFamily: "\"Raleway\", sans-serif"
    }
}))

export default (props) => {
    const classes = useStyles();

    const [email, setEmail] = React.useState(null)

    const { 
        handleSetContactEmail, 
        handleDialogClickOpen,
        sendGaEvent,
    } = props;

    const handleInput = (event) => {
        setEmail(event.target.value)
        handleSetContactEmail(event.target.value)
    }

    const handleEmailSubmit = () => {
        sendGaEvent('Contact', 'User Click', email ? 'With Email' : 'Without Email', email)
        handleDialogClickOpen();
    };

    return (
        <Grid container direction="row" justify="center" alignItems="center" item sm={6} className={classes.root} >
            <Grid item xs={12}>
                <Typography gutterBottom variant="h2" className={classes.header}>
                    Your Contact for Hydrospatial Solutions
                </Typography>
            </Grid>
            <Grid item xs={12}>
                <div>
                    <TextField 
                        className={classes.textField} 
                        id="outlined-basic" 
                        label="Enter your email" 
                        variant="outlined" 
                        value={email}
                        onChange={handleInput}
                    />
                    <Button 
                        className={classes.button}
                        onClick={handleEmailSubmit}
                    >
                        Get in Touch
                    </Button>
                </div>
                
            </Grid>
        </Grid>
    )
};