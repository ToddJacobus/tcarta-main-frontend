import React from 'react';

import { fetchHeroPageCarouselContent } from '../../actions';

import Carousel from 'react-material-ui-carousel'
import { Paper, Typography, makeStyles } from '@material-ui/core';
import { connect } from 'react-redux';

const useStyles = makeStyles(theme => ({
    root: {
        margin: 5,
        width: '100%',
        maxWidth: 1000,
    },
    image: {
        height: '100%',
        width: '100%',
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        borderRadius: 5
    },
    caption: {
        position: 'absolute',
        zIndex: 999,
        marginRight: 'auto',
        padding: 5,
        left: 0,
        right: 0,
        top: '66%',
        textAlign: 'left',
        width: '60%',
        height: 75,
        backgroundColor: 'white',
        borderRadius: '0px 5px 5px 0px',
        [theme.breakpoints.down('xs')]: {
            height: 45
        },
    },
    captionText: {
        marginTop: 3,
        marginBottom: 3,
        marginRight: 10,
        textAlign: 'justify',
        fontFamily: "\"Cabin\", sans-serif",
        [theme.breakpoints.down('xs')]: {
            fontSize: "10px"
        },
    },
    button: {
        background: 'linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
    loadingItem: {
        height: 400,
        width: 400,
        display: 'block',

    },
}))


const Item = (props) => {
    const classes = useStyles();

    const { 
        link, 
        url, 
        caption, 
        tagline,
        sendGaEvent,
    } = props;

    return (
        <Paper className={classes.item}>
            <a 
                href={link} 
                target="_blank" 
                rel="noopener noreferrer"
                onClick={() => sendGaEvent("Carousel", "User Click", link)}
            >
                <img 
                    className={classes.image} 
                    src={url} 
                    alt={caption} 
                />
            </a>
            
            <div className={classes.caption}>
                <Typography variant="h6" className={classes.captionText}>{tagline}</Typography>
            </div>
        </Paper>
    )    
}


const HeroCarousel = (props) => {
    const classes = useStyles();

    const { fetchHeroPageCarouselContent, hero_carousel_content } = props;

    React.useEffect(() => {
        fetchHeroPageCarouselContent();
    }, [fetchHeroPageCarouselContent]);

    return (
        <Carousel className={classes.root}>
            {
                hero_carousel_content && hero_carousel_content.items.length ?
                    hero_carousel_content.items.map( (item, id) => (
                        <Item {...item} {...props} key={id}/>
                    ))

                : <div />
            }
        </Carousel>
    )
};

const mapStateToProps = (state) => {
    return {
        hero_carousel_content: state.hero_carousel_content
    }
}

export default connect(
    mapStateToProps,
    {
        fetchHeroPageCarouselContent,
    }
)(HeroCarousel);