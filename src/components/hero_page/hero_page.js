import React from 'react';

import { Link } from "react-router-dom";

import { makeStyles } from '@material-ui/core/styles';
import {
    Grid,
    Fab,
} from '@material-ui/core';

import ReactGA from 'react-ga';

import Banner from './banner';
import HeroCarousel from './heroCarousel'

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        minHeight: "-webkit-fill-available",
        marginBottom: '20%',
    },
    logo: {
        width: '100%',
        maxWidth: 450,
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 15,
        marginBottom: 15
    },
    logoContainer: {},
    bannerContainer: {
        maxWidth: 1000,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    carouselContainer: {
        margin: 'auto'
    },
    kidsCornerFabRoot: {
        position: 'fixed',
        zIndex: 9999,
        fontSize: 12,
        borderRadius: '10%',
        bottom: 5,
        right: 5,
        [theme.breakpoints.down(600)]: {
            top: 5,
            left: 5,
        },
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
    },
    kidsCornerFabLabel: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        marginLeft: 2,
        marginRight: 2,
    },
    kids: {
        fontSize: 14,
    },
    corner: {
        fontSize: 12,
    },
}))


export default (props) => {
    const classes = useStyles();

    const { handleNavbarIndicatorChange } = props;

    React.useEffect(() => {
        handleNavbarIndicatorChange(0)
    }, [handleNavbarIndicatorChange]);


    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    return (
        <div className={classes.root} >
            <Fab 
                className={classes.kidsCornerFabRoot}
                classes={{
                    label: classes.kidsCornerFabLabel
                }}
                component={Link}
                to="/kids"
            >
                <div className={classes.kids}>Kids</div>
                <div className={classes.corner}>Corner</div>
            </Fab>
            <Grid container spacing={0} direction="column" justify="center">
                <Grid className={classes.carouselContainer} container item xs={12} spacing={3} justify="center">
                    <HeroCarousel {...props} />
                </Grid>
                <Grid container direction="row" item xs={12} spacing={1} justify="center" alignItems="center" className={classes.bannerContainer}>
                    <Banner {...props}/>
                    <Grid container direction="row" justify="center" alignItems="center" item sm={6} className={classes.logoContainer}>
                        <Grid item sm={12}>
                            <img className={classes.logo} src="//images.ctfassets.net/m7umyxqhfu1m/7si3pznQpRzqEYu1u3zExd/c18323edbfbadf7bdf15ca8b17fadd63/TCARTA_Logo_HiRes_Blue.png" alt="TCarta Logo"/>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
};
