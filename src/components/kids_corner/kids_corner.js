
import React from 'react';
import { connect } from 'react-redux';
import uuid from 'react-uuid';

import { 
    makeStyles, 
    GridList, 
    IconButton, 
    GridListTile, 
    GridListTileBar,
    Typography,
    Button,
    Dialog,
    DialogContent,
    DialogActions,
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';

import ReactGA from 'react-ga';

import {
    fetchKidsCornerPosts
} from '../../actions';

import RenderContentfulRichText from '../news_page/rtfRenderer';


const ContentDialog = props => {
    const {
        open, 
        setDialogOpen,
        dialogContent,
        assets,
        sendGaEvent,
    } = props;
    return (
        <div>
            <Dialog
                open={open}
                onClose={() => setDialogOpen(false)}
                maxWidth="md"
                fullWidth={true}
            >
                <DialogContent>
                    <div
                        style={{
                            width: '100%',
                            height: '100%',
                            textAlign: 'center',
                        }}
                    >
                        <div>
                            { 
                                RenderContentfulRichText(
                                    dialogContent,
                                    assets,
                                    sendGaEvent,
                                )
                            }
                        </div>
                    </div>
                </DialogContent>

                <DialogActions>
                    <Button
                        onClick={() => setDialogOpen(false)}
                    >
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
};


const KidsCorner = props => {
    const { 
        kids_corner_content, 
        sendGaEvent,
        fetchKidsCornerPosts,
    } = props;

    const LOGO_URL = "https://images.ctfassets.net/m7umyxqhfu1m/4ClhcackktufmBxzndVQMH/a3a2d28c069e4308d5e63ec98638fe9d/TCARTA__Bulletpoint.png?h=250";

    const CALLS_TO_ACTION = [
        {
            id: uuid(),
            title: "Explore TCarta",
            link: "https://www.explore.tcarta.com",
            featured: false,
            url: LOGO_URL,
        },
    ]

    const [items, setItems] = React.useState();
    const [fillerNeeded, setFillerNeeded] = React.useState(false);

    const constructItemList = (data) => {
        // NOTE: this does not take into account "featured" items,
        //       that take up 2 columns.  We need to count the featured
        //       items, then add this to the length.
        let items = [ ...data ]; // create new variable reference

        // Get number of featured items
        const numberFeatured = items.filter(item => item.featured).length

        // Calculate the total number of tiles
        let numberTiles = items.length + numberFeatured

        // iterate through calls to action array
        for (let index = 0; index < CALLS_TO_ACTION.length; index++) {
            // where to insert COA items?
            const spacing = 3; // i.e., every Nth item
            // splice in item at location, accounting for off-by-one errors
            items.splice((index + 1)*(spacing - 1), 0, CALLS_TO_ACTION[index])
            // Calculate the number of tiles used by the imported content
            numberTiles = items.length + numberFeatured
        }
        // if the number of tiles is not divisible by 3 (i.e., there are spaces)
        if (numberTiles%3) {
            // Calculate spaces that need to be filled
            const spaces = 3-(numberTiles%3);
            // For each space that needs to be filled...
            for (let index = 0; index < spaces; index++) {
                // Calculate how to space out content evenly
                const spacing = items.length/(spaces + 1)
                // define filler item
                const fillerItem = {
                    id: uuid(),
                    title: "Filler content!",
                    featured: false,
                    url: LOGO_URL,
                }
                // place filler item at spacing, paginated by the index
                items.splice(spacing*(index + 1), 0, fillerItem)
                // numberTiles = items.length + numberFeatured
                
            }
        }

        return items
    };

    React.useEffect(() => {
        
        if (kids_corner_content) {
            setItems(constructItemList(kids_corner_content.items))
        }
        
    }, [kids_corner_content])

    React.useEffect(() => {
        fetchKidsCornerPosts();
    }, []);

    
    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);
 

    const useStyles = makeStyles(theme => {

        return (
            {
                root:{
                    display: 'flex',
                    flexWrap: 'wrap',
                    justifyContent: 'space-around',
                    overflow: 'hidden',
                    borderTop: '1px solid #474747',
                    paddingTop: 50,
                    width: "90%",
                    marginRight: 'auto',
                    marginLeft: 'auto',
                    maxWidth: 1200,
                },
                gridList: {
                    width: '100%',
                    maxWidth: 750,
                    height: "100%",
                    transform: 'translateZ(0)',
                },
                imageTile: {
                    borderRadius: 5,
                },
                modalImage: {
                    width: '100%',
                    border: '1px dashed red',
                },
                title: {
                    fontFamily: "\"Cabin\", sans-serif",
                    fontSize: "1rem",
                    whiteSpace: 'normal',
                    [theme.breakpoints.down(500)]: {
                        fontSize: "0.7rem",
                    }
                },
                featuredTitle: {
                    fontFamily: "\"Cabin\", sans-serif",
                    fontSize: "1rem",
                },
                icon: {
                    color: 'white',
                },
                headerContainer: {
                    width: "100%",
                    textAlign: 'left',
                    marginTop: 90,
                    marginBottom: 45,
                    marginLeft: 20,
                    marginRight: 20,
                    textalign: 'justify',
                    
                },
                header: {
                    fontFamily: "\"Raleway\", sans-serif",
                },
                subHeader: {
                    fontFamily: "\"Cabin\", sans-serif",
                    width: "66%",
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginTop: 50,
                },
            }
    )})


    const classes = useStyles();


    const [dialogOpen, setDialogOpen] = React.useState(false);
    const [dialogContent, setDialogContent] = React.useState();

    const handleSetDialogOpen = (children) => {
        setDialogOpen(true)
        // setDialogChildren(<div>{ children }</div>)
    };

    const handleTileClick = (event, tile, assets, sendGaEvent) => {
        // sendGaEvent('Gallery Image Details Modal', 'User Click', tile.id);
        setDialogOpen(true);
        setDialogContent(tile)
    };

    return (
        <div>
            {
                kids_corner_content && dialogContent ?
                <ContentDialog 
                    // open, 
                    // setDialogOpen,
                    // dialogContent,
                    // assets,
                    // sendGaEvent,
                    open={dialogOpen}
                    setDialogOpen={setDialogOpen}
                    dialogContent={dialogContent.content}
                    assets={kids_corner_content.assets}
                />   
                : <div /> 
            }
            
            <div className={classes.headerContainer}>
                <Typography variant="h3" className={classes.header} >Kids Corner</Typography>
                <Typography variant="h6" className={classes.subHeader} >Fun, educational, real-life assignments for kids of all ages! Share your work! Email us something you're proud of to kids@tcarta.com and we can feature it here!</Typography>
            </div>
            <div className={classes.root}>
                {
                    kids_corner_content ?
                    <GridList cellHeight={200} spacing={8} cols={2} className={classes.gridList}>
                        {kids_corner_content.items.map((tile) => (
                            <GridListTile 
                                key={tile.id} 
                                cols={tile.featured ? 2 : 1}
                                rows={tile.featured ? 2 : 1}
                                classes={{
                                    tile: classes.imageTile,
                                }}
                                onClick={(event) => handleTileClick(event, tile, kids_corner_content.assets, sendGaEvent)}
                            >
                                <img src={tile.url} alt={tile.title} />
                                <GridListTileBar 
                                    title={tile.title}
                                    actionIcon={
                                        tile.longLink || tile.link?
                                        <a 
                                            href={tile.longLink ? tile.longLink : tile.link} 
                                            target="_blank" 
                                            rel="noopener noreferrer"
                                            onClick={() => sendGaEvent("Kids Corner Reference Link", "User Click", tile.id)}
                                        >
                                            <IconButton 
                                                aria-label={`info about ${tile.title}`} 
                                                className={classes.icon}
                                            >
                                                <InfoIcon />
                                            </IconButton>
                                        </a>
                                        : <div />
                                    }
                                    classes={{
                                        title: tile.featured ? classes.featuredTitle : classes.title,
                                    }}
                                />
                            </GridListTile>
                        ))}
                    </GridList>
                    : <div />
                }
            </div>
        </div>
    )
};

const mapStateToProps = state => {
    return {
        kids_corner_content: state.kids_corner_content,
    }
};

export default connect(
    mapStateToProps,
    {
        fetchKidsCornerPosts,
    }
)(KidsCorner);