import React, { useEffect } from 'react';
import { connect } from 'react-redux';

import { Link as RouterLink } from "react-router-dom";

import { 
    documentToReactComponents 
} from '@contentful/rich-text-react-renderer';

import { makeStyles } from '@material-ui/core/styles';
import { 
    Grid, 
    Typography, 
    Paper, 
    Link 
} from '@material-ui/core';
import { 
    Phone,
} from '@material-ui/icons';
import BusinessIcon from '@material-ui/icons/Business';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import FacebookIcon from '@material-ui/icons/Facebook';

import { fetchLegalDocuments } from '../actions';

const useStyles = makeStyles(theme => ({
    root: {
        position: 'relative',
        zIndex: 2,
        width: '100%',
        maxWidth: 1200,
        marginTop: 50,
        marginLeft: 'auto',
        marginRight: 'auto',
        [theme.breakpoints.down('xs')]: {
            marginBottom: 75,
        },
    },
    pannel: {
        width: 300,
        maxWidth: 500,
        textAlign: 'left',
        marginRight: 'auto',
        marginLeft: 'auto',
    },
    footerMenu: {
        marginTop: 40,
        padding: '0.5em 0',
    },
    linkContainer: {
        display: 'flex',
        justifyContent: 'space-evenly',
        flexWrap: 'wrap',
        padding: 0,
        margin: 0,
        textAlign: 'center',
    },
    link: {
        display: 'block',
        padding: '0.25em 1em',
        textAlign: 'center',
        color: 'black',
    },
    infoTable: {
        width: '100%',
    },
    rowIcon: {
        verticalAlign: 'text-top',
    },
    logoContainer: {
        width: '100%',
        marginRight: 'auto',
        paddingBottom: 20,
        paddingTop: 5,
        textAlign: 'center',
    },
    logo: {
        width: '100%',
        maxWidth: 200,
        height: 'auto',
    },
    socialIconsContainer: {
        width: '100%',
    },
    socialIcon: {
        textAlign: 'center',
    },
    anchor: {
        color: 'black',
    },
    legalHeader: {
        fontFamily: "\"Raleway\", sans-serif",
        marginTop: 40,
        marginBottom: 20,
    },
    legalBody: {
        fontFamily: "\"Cabin\", sans-serif",
    },
}))


const LegalDocs = (props) => {
    const classes = useStyles();

    const { legal_documents_content } = props;

    return (
        <div className={classes.legalDocsRoot}>
            {
                legal_documents_content ?
                legal_documents_content.items.map((item, id) => {
                    const { title, body } = item;
                    return (
                        <div key={id}>
                            <Typography className={classes.legalHeader} variant="h4">{title}</Typography>
                            <Typography className={classes.legalBody} variant="body1">
                                {documentToReactComponents(body)}
                            </Typography>
                        </div>
                    )
                }) : <div></div>
            }
        </div>
    )
};

const Footer = (props) => {
    const classes = useStyles();

    const { 
        fetchLegalDocuments, 
        handleDialogClickOpen, 
        handleTextDialogClickOpen,
        sendGaEvent,
    } = props;

    useEffect(() => {
        fetchLegalDocuments();
    }, []);

    const handleContactClick = () => {
        sendGaEvent('Contact', 'User Click', 'Footer')
        handleDialogClickOpen();
    };

    return (
        <Paper
            className={classes.root}
        >
            <Grid container spacing={2} justify="center" alignItems="flex-start">
                <Grid item md={4} >
                    <div className={classes.pannel}>
                        <Typography variant="h6">TCarta Marine LLC</Typography>
                        <table className={classes.infoTable}>
                            <tr>
                                <td className={classes.rowIcon}><BusinessIcon /></td>
                                <td>
                                    <Typography variant="subtitle1">1015 Federal Boulevard</Typography>
                                    <Typography variant="subtitle1">Denver, CO 80204, USA</Typography>
                                </td>
                            </tr>
                            <tr>
                                <td className={classes.rowIcon}><Phone /></td>
                                <td><Typography variant="subtitle1">+1 (303) 284-6144</Typography></td>
                            </tr>
                        </table>
                    </div>
                </Grid>
                <Grid item md={4}>
                    <div className={classes.pannel}>
                        <Typography variant="h6">TCarta Caribe LLC</Typography>
                        <table className={classes.infoTable}>
                            <tr>
                                <td className={classes.rowIcon}><BusinessIcon/></td>
                                <td>
                                    <Typography variant="subtitle1">9th Floor, PanJam Building</Typography>
                                    <Typography variant="subtitle1">60 Knutsford Boulevard</Typography>
                                    <Typography variant="subtitle1">New Kingston, Jamaica</Typography>
                                </td>
                            </tr>
                            <tr>
                                <td className={classes.rowIcon}><Phone/></td>
                                <td><Typography variant="subtitle1">+1 (876) 817-8567</Typography></td>
                            </tr>
                        </table>
                    </div>
                </Grid>
                <Grid item md={4}>
                    <div className={classes.logoContainer}>
                        <a href="https://explore.tcarta.com" target="_blank" rel="noopener noreferrer">
                            <img 
                                src="https://images.ctfassets.net/m7umyxqhfu1m/78pNW2XkwianCRBknlfIl4/d0029a4b6fe33267e19c33e2ce7dcc88/TCARTA_Explore_Logo_500x224.png"
                                alt="TCarta Logo"
                                className={classes.logo}
                            />
                        </a>
                        
                    </div>
                    <table className={classes.socialIconsContainer}>
                        <tr>
                            <td className={classes.socialIcon}>
                                <a 
                                    href="https://www.linkedin.com/company/2385157?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A2385157%2Cidx%3A2-1-2%2CtarId%3A1486997780192%2Ctas%3Atcarta%20ma"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className={classes.anchor}
                                    onClick={() => sendGaEvent('Social Media Link', 'User Click', 'LinkedIn')}
                                >
                                    <LinkedInIcon />
                                </a>
                            </td>
                            <td className={classes.socialIcon}>
                                <a
                                    href="https://www.instagram.com/tcarta.marine"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className={classes.anchor}
                                    onClick={() => sendGaEvent('Social Media Link', 'User Click', 'Instagram')}
                                >
                                    <InstagramIcon />
                                </a>
                                
                            </td>
                            <td className={classes.socialIcon}>
                                <a
                                    href="https://www.facebook.com/TCarta.MarineLandAir/"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className={classes.anchor}
                                    onClick={() => sendGaEvent('Social Media Link', 'User Click', 'Facebook')}
                                >
                                    <FacebookIcon />
                                </a>
                                
                            </td>
                            <td className={classes.socialIcon}>
                                <a
                                    href="http://www.twitter.com/tcarta_"
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    className={classes.anchor}
                                    onClick={() => sendGaEvent('Social Media Link', 'User Click', 'Twitter')}
                                >
                                    <TwitterIcon />
                                </a>
                                
                            </td>
                        </tr>
                    </table>
                </Grid>
            </Grid>
            <div className={classes.footerMenu}>
                <Typography className={classes.linkContainer}>
                    <Link 
                        className={classes.link}
                        onClick={handleContactClick}
                    >
                        Contact
                    </Link>
                    <Link 
                        className={classes.link}
                        component={RouterLink}
                        to={'/about'}
                    >
                        About
                    </Link>
                    <Link 
                        className={classes.link}
                        onClick={() => {
                            sendGaEvent('Legal', 'User Click', 'Footer');
                            handleTextDialogClickOpen(
                                <LegalDocs {...props}/>
                            );
                        }}
                    >
                        Legal
                    </Link>
                </Typography>
            </div>
        </Paper>
    )
}

const mapStateToProps = (state) => {
    return {
        legal_documents_content: state.legal_documents_content,
    }
};

export default connect(
    mapStateToProps,
    {
        fetchLegalDocuments,
    }
)(Footer);