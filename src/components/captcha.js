import React, { Component } from 'react';
import { ReCaptcha } from 'react-recaptcha-google'

import Button from '@material-ui/core/Button';
import { withStyles  } from '@material-ui/core/styles';


const styles = {
    button: {
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    }
}


class Captcha extends Component {
  constructor(props, context) {
    super(props, context);
    this.onLoadRecaptcha = this.onLoadRecaptcha.bind(this);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.handleCaptchaCallenge = this.handleCaptchaCallenge.bind(this);
  }
  componentDidMount() {
    if (this.captcha) {
        this.captcha.reset();
    }
  }

  onLoadRecaptcha() {
      if (this.captcha) {
          this.captcha.reset();
      }
  }

  handleCaptchaCallenge() {
    if (!this.props.firstName) {
      this.props.setFirstNameError(true)
    }

    if (!this.props.lastName) {
      this.props.setLastNameError(true)
    }

    if (!this.props.email) {
      this.props.setEmailError(true)
    };
    
    if (
        this.props.email && 
        this.props.firstName && 
        this.props.lastName && 
        !this.props.emailError
        ) {
            this.captcha.reset();
            this.captcha.execute();
          }
  }

  verifyCallback(recaptchaToken) {
      this.props.handleFormSubmit({'g-recaptcha-response':recaptchaToken})
  }


  render() {
    return (
      <div>
        <Button
            variant="contained"
            onClick={this.handleCaptchaCallenge}
            disabled={false}
            className={this.props.classes.button}
        >
            Submit
        </Button>
        <ReCaptcha
            ref={(el) => {this.captcha = el;}}
            size="invisible"
            render="explicit"
            sitekey="6LeeGNsUAAAAAC4yIpbs3-a4-XJb7pBrcXBzHt6n"
            onloadCallback={this.onLoadRecaptcha}
            verifyCallback={this.verifyCallback}
        />
      </div>
    );
  };
};
export default withStyles(styles)(Captcha);