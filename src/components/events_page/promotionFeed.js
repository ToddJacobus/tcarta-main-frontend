import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {
    List,
    Divider,
    Typography,
} from '@material-ui/core';

import Promotion from './promotion';


const useStyles = makeStyles((theme) => ({
    root: {

    },
    header: {
        fontFamily: "\"Cabin\", sans-serif",
        borderBottom: "1px solid #FAFAFA",
        paddingBottom: 5,
    },
}));

const PromotionFeed = props => {
    const classes = useStyles();
    const {
        items,
    } = props;

    return (
        <div>
            <Typography variant="h5" className={classes.header}>The Latest Updates</Typography>
            <List className={classes.root}>
                {
                    items ?
                    items.map((item, index) => {
                        return (
                            <div>
                                <Promotion {...item} key={item.id} />
                                {
                                    index < items.length - 1 ?
                                    <Divider variant="inset" component="li" />
                                    : <div />
                                }
                            </div>
                        )
                    })
                    : <div />
                }
            </List>
        </div>
    )
};

export default PromotionFeed;