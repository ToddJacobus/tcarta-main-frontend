import React from 'react';
import { connect } from 'react-redux';
import { 
    makeStyles, 
    Paper,
    Typography,
    Grid,
    Button,
} from '@material-ui/core';

import ReactGA from 'react-ga';

import RenderContentfulRichText from '../news_page/rtfRenderer';

import {
    fetchEvents,
} from '../../actions';

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: 20,
        marginLeft: 'auto',
        marginRight: 'auto',
        marginBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
        maxWidth: 800,
    },
    headerContainer: {
        width: '100%',
        maxWidth: 800,
        [theme.breakpoints.down('xs')]: {
          marginTop: 60,  
        },
        marginTop: 85,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    titleContainer: {
        width: '100%',
        minHeight: 500,
        backgroundColor: '#474747',
        color: '#FAFAFA',
        position: 'relative',
        marginBottom: 3,
    },
    title: {
        position: 'absolute',
        top: "15%",
        width: '85%',
        marginLeft: 5,
        fontFamily: "\"Raleway\", sans-serif",
    },
    subTitle: {
        position: 'absolute',
        top: "75%",
        width: '80%',
        marginLeft: 5,
        fontFamily: "\"Cabin\", sans-serif",
    },
    heroImageContainer: {
        width: '100%',
        textAlign: 'center',
    },
    actionContainer: {
        width: '100%',
        height: 500,
        position: 'relative',
        marginBottom: 3,
    },
    actionButtonContainer: {
        width: '100%',
        position: 'absolute',
        bottom: '10%',
        textAlign: 'center',
    },
    actionDescriptionContainer: {
        width: '100%',
        position: 'absolute',
        bottom: '25%',
    },
    actionDescription: {
        width: '100%',
        maxWidth: 300,
        fontFamily: "\"Cabin\", sans-serif",
        textAlign: 'justify',
        marginRight: 'auto',
        marginLeft: 'auto',
        paddingLeft: 10,
        paddingRight: 10,
    },
    button: {
        marginTop: 20,
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    label: {
        marginTop: 15,
    },
}))

const Item = props => {
    const { 
        item, 
        linkedAssets,
        sendGaEvent,
    } = props;

    return (
        <div>
            {
                RenderContentfulRichText(
                    item.presentationMaterials,
                    linkedAssets,
                    sendGaEvent,
                )
            }
        </div>

    )
};

const EventDetail = props => {
    const classes = useStyles();
    const { 
        match, 
        events_content, 
        fetchEvents, 
        handleNavbarIndicatorChange,
        sendGaEvent,
    } = props;

    const item = events_content ? events_content.items[0] : undefined;

    React.useEffect(() => {
        fetchEvents({slug: match.params.slug})
    }, [match.params.slug, fetchEvents]);

    React.useEffect(() => {
        handleNavbarIndicatorChange(2);
    }, [handleNavbarIndicatorChange]);

    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    return (
        <div>
            <Grid container className={classes.headerContainer}>
                <Grid item sm={6} className={classes.titleContainer}>
                    <Typography variant="h3" className={classes.title}>
                        { item ? item.title : "" }
                    </Typography>
                    <Typography variant="h6" className={classes.subTitle}>
                        { item ? item.subTitle : ""}
                    </Typography>

                </Grid>
                <Grid item sm={6} className={classes.actionContainer}>
                    <div className={classes.heroImageContainer}>
                        <img src={item ? item.heroImageUrl : ""} alt={item ? item.title: ""} className={classes.heroImage}/>
                    </div>
                    <div className={classes.actionDescriptionContainer}>
                        <Typography className={classes.actionDescription} variant="subtitle1">Click below to schedule a time to meet with us</Typography>
                    </div>
                    <div className={classes.actionButtonContainer}>
                        <Button
                            className={classes.button}
                            classes={{
                                label: classes.label,
                            }}
                            href={item ? item.calendlyUrl : ''}
                            target="_blank"
                            onClick={() => sendGaEvent("Contact", "User Click", "Appointment Scheduler")}
                        >
                            Let's Chat
                        </Button>
                    </div>
                </Grid>
            </Grid>
                
            <Paper className={classes.root}>
                {
                    events_content ?
                    <Item item={item} linkedAssets={events_content.linkedAssets} {...props} />
                    : <div />
                }
            </Paper>    
        </div>
        
    )
};

const mapStateToProps = state => {
    return {
        events_content: state.events_content,
    };
};

export default connect (
    mapStateToProps,
    {
        fetchEvents,
    }
)(EventDetail);