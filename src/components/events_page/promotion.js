import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import {
    ListItem,
    ListItemText,
    Typography,
    ListItemAvatar,
    Link,
    Avatar,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {

    },
    imageContainer: {
        width: 50,
        height: 50,
        textAlign: 'center',
    },
    image: {
        width: 75,
        marginRight: 10,
    },
    itemLink: {
        color: 'white',
    },
}));

const Promotion = props => {
    const classes = useStyles();
    const {
        title,
        description,
        image,
        publicationDateString,
        link,
    } = props;

    return (
        <Link href={link} target={link.search("tcarta.com") > 0 ? "" : "_blank" } rel="noopener noreferrer" className={classes.itemLink}>
            <ListItem button alignItems="flex-start">
                <ListItemAvatar>
                    <img src={image} alt={title} className={classes.image} />
                </ListItemAvatar>
                <ListItemText 
                    primary={publicationDateString}
                    secondary={
                        <Typography>
                            {description ? description : ""}
                        </Typography>
                    }
                />
            </ListItem>
        </Link>
        
    )
};

export default Promotion;