import React from 'react';
import { 
    makeStyles,
    ExpansionPanel, 
    ExpansionPanelSummary, 
    ExpansionPanelDetails,
    Typography,
    Grid,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles(theme => {
    return ({
        root: {},
        column: {
            flexBasis: '66.66%',
        },
        headingContainer: {
            width: 275,
        },
        heading: {
            fontSize: 20,
            [theme.breakpoints.down('sm')]: {
                fontSize: 15,
            },
            fontFamily: "\"Cabin\", sans-serif",
        },
        subHeading: {
            fontSize: 20,
            fontFamily: "\"Cabin\", sans-serif",
            [theme.breakpoints.down('sm')]: {
                fontSize: 15,
            }

        },
    })
})

const FutureEvent = props => {
    const { 
        item,
        sendGaEvent,
    } = props;

    const classes = useStyles();

    return (
        <ExpansionPanel className={classes.root}>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls="panel1-content"
                onClick={() => sendGaEvent("Future Event Expansion Pannel", "User Click", item.id)}
            >
                <Grid container spacing={1}>
                    <Grid item sm={6} className={classes.headingContainer}>
                        <Typography 
                            variant="subtitle1"
                            className={classes.heading}    
                        >
                            {item.title}
                        </Typography>
                    </Grid>
                    <Grid item sm={6} className={classes.headingContainer}>
                        <Typography 
                            variant="subtitle1"
                            className={classes.subHeading}
                        >
                            {`${item.startDate} to ${item.endDate}`}
                        </Typography>
                    </Grid>
                </Grid>
                
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Typography variang="body">
                    {item.description}
                </Typography>
            </ExpansionPanelDetails>
        </ExpansionPanel>  
        
    )
};

export default FutureEvent;