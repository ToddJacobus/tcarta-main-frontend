import React from 'react';
import { connect } from 'react-redux';
import {
    makeStyles,
    Typography,
    Button,
    Grid,
    Drawer,
    Fab,
    List,
    ListItem,
    ListItemText,
    Divider,
    Hidden,
    Paper,
} from '@material-ui/core';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import ListIcon from '@material-ui/icons/List';

import { Link, animateScroll as scroll } from "react-scroll";

import ReactGA from 'react-ga';

import RenderContentfulRichText from '../news_page/rtfRenderer';

import {
    fetchEventDetailitems,
    fetchEvents,
} from '../../actions';

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexDirection: 'column',
        maxWidth: 1200,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    headerContainer: {
        width: '100%',
        [theme.breakpoints.down('xs')]: {
          marginTop: 60,  
        },
        marginTop: 85,
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    titleContainer: {
        width: '100%',
        minHeight: 500,
        backgroundColor: '#474747',
        color: '#FAFAFA',
        position: 'relative',
        marginBottom: 3,
    },
    title: {
        position: 'absolute',
        top: "15%",
        width: '85%',
        marginLeft: 5,
        fontFamily: "\"Raleway\", sans-serif",
    },
    subTitle: {
        position: 'absolute',
        top: "75%",
        width: '80%',
        marginLeft: 5,
        fontFamily: "\"Cabin\", sans-serif",
    },
    heroImageContainer: {
        width: '100%',
        textAlign: 'center',
    },
    actionContainer: {
        width: '100%',
        height: 500,
        position: 'relative',
        marginBottom: 3,
    },
    actionButtonContainer: {
        width: '100%',
        position: 'absolute',
        bottom: '10%',
        textAlign: 'center',
    },
    actionDescriptionContainer: {
        width: '100%',
        position: 'absolute',
        bottom: '25%',
    },
    actionDescription: {
        width: '100%',
        maxWidth: 300,
        fontFamily: "\"Cabin\", sans-serif",
        textAlign: 'justify',
        marginRight: 'auto',
        marginLeft: 'auto',
        paddingLeft: 10,
        paddingRight: 10,
    },
    button: {
        marginTop: 20,
        background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
        border: 0,
        borderRadius: "20px 0px 20px 0px",
        boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
        color: 'white',
        height: 55,
        padding: '0 30px',
        display: 'inline-block',
        verticalAlign: 'middle',
        margin: 4
    },
    label: {
        marginTop: 15,
    },
    mainContentContainer: {
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: 'auto',
            paddingLeft: 5,
            paddingRight: 5,
        },
    },
    itemRoot: {
        marginTop: 50,
        borderTop: '1px solid black',
    },
    itemHeader: {
        marginTop: 10,
        fontFamily: "\"Raleway\", sans-serif",
        [theme.breakpoints.down('sm')]: {
            fontSize: 50,
        },
    },
    itemDate: {
        marginLeft: 10,
        fontFamily: "\"Cabin\", sans-serif",
    },
    itemSubHeader: {
        marginLeft: 10,
        maxWidth: 600,
        marginTop: 50,
        marginBottom: 50,
        fontFamily: "\"Cabin\", sans-serif",
    },
    itemBodyContainer: {
        maxWidth: 800,
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingLeft: 5,
        paddingRight: 5,
        fontFamily: "\"Cabin\", sans-serif",
        fontSize: 16,
    },
    drawerPaper: {
        width: drawerWidth,
        zIndex: 1,
        [theme.breakpoints.up('sm')]: {
            paddingTop: 60,
        },
    },
    sideNavCloseIcon: {
        marginLeft: 'auto',
        marginRight: 10,
    },
    sideNavToggle: {
        position: 'fixed',
        zIndex: '1',
        left: 20,
        top: 90,
        color: 'white',
        backgroundColor: '#00A3E0',
        [theme.breakpoints.down('sm')]: {
            top: 20,
        }
    },
}));

const Item = props => {
    const classes = useStyles();
    const {
        id,
        header,
        subHeader,
        body,
        date,
        linkedAssets,
        sendGaEvent,
    } = props;

    return (
        <div className={classes.itemRoot}>
            <Typography name={id} className={classes.itemHeader} variant="h2">{header}</Typography>
            {date ? <Typography className={classes.itemDate} variant="subtitle1" >{date}</Typography> : <div />}
            <Typography className={classes.itemSubHeader} variant="h5">{subHeader}</Typography>
            <div className={classes.itemBodyContainer}>
                {
                    RenderContentfulRichText(
                        body,
                        linkedAssets,
                        sendGaEvent,
                    )
                }
            </div>
        </div>
    )
};

const SideNavList = props => {
    const classes = useStyles();
    const {
        items,
        sendGaEvent,
    } = props;

    return (
        <List 
            component="nav" 
        >
            {
                items ?
                items.map(item => {
                    return (
                        <div>
                            <ListItem 
                                component={Link}
                                button
                                activeClass="active"
                                spy={true}
                                smooth={true}
                                offset={-75}
                                duration={500}
                                to={item.id}
                                onClick={() => sendGaEvent("Navigation", "User Click", item.header)}
                            >
                                <ListItemText primary={item.header} />
                            </ListItem>
                            <Divider />
                        </div>
                        
                    )
                })
                : <div />
            }
        </List>
        
    )
    
}

const EventDetailList = props => {
    const classes = useStyles();
    const {
        match,
        events_content,
        event_details_content,
        fetchEventDetailitems,
        fetchEvents,
        sendGaEvent,
        handleNavbarIndicatorChange,
    } = props;

    const event = events_content ? events_content.items[0] : undefined;

    React.useEffect(() => {
        handleNavbarIndicatorChange(2)
    }, [handleNavbarIndicatorChange])

    React.useEffect(() => {
        fetchEvents({slug: match.params.slug});
        fetchEventDetailitems({slug: match.params.slug});
    }, [fetchEventDetailitems, fetchEvents, match.params.slug])

    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    const [open, setOpen] = React.useState(false);

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <div className={classes.root}>
            <Hidden smUp>
                <Fab 
                    size="large"
                    className={classes.sideNavToggle}
                    onClick={() => setOpen(open ? false : true)}
                >
                    <ListIcon />
                </Fab>
            </Hidden>
            
           
            <Hidden smUp>
                <div className={classes.sideNavRootMobile}>
                    <Drawer
                        classes={{
                            paper: classes.drawerPaper
                        }}
                        anchor="left"
                        open={open}
                        onClose={() => setOpen(false)}
                    >
                        
                        <Button onClick={() => setOpen(false)} >
                            <NavigateBeforeIcon 
                                classes={{
                                    root: classes.sideNavCloseIcon
                                }} 
                            />
                        </Button>
                        <SideNavList 
                            items={event_details_content ? event_details_content.items : []}
                            sendGaEvent={sendGaEvent}
                        />

                    </Drawer>
                </div>    
            </Hidden>
            <Hidden xsDown>
                    <Drawer
                        variant="permanent"
                        classes={{
                            paper: classes.drawerPaper
                        }}
                        container={container}
                        elevation={0}
                    >
                        <SideNavList 
                            items={event_details_content ? event_details_content.items : []}
                            sendGaEvent={sendGaEvent}
                        />
                    </Drawer>

            </Hidden>
            <div className={classes.mainContentContainer}>
                <Grid container className={classes.headerContainer}>
                    <Grid item sm={6} className={classes.titleContainer}>
                        <Typography variant="h3" className={classes.title}>
                            { event ? event.title : "" }
                        </Typography>
                        <Typography variant="h6" className={classes.subTitle}>
                            { event ? event.subTitle : ""}
                        </Typography>

                    </Grid>
                    <Grid item sm={6} className={classes.actionContainer}>
                        <div className={classes.heroImageContainer}>
                            <img src={event ? event.heroImageUrl : ""} alt={event ? event.title: ""} className={classes.heroImage}/>
                        </div>
                        <div className={classes.actionDescriptionContainer}>
                            <Typography className={classes.actionDescription} variant="subtitle1">Click below to schedule a time to meet with us</Typography>
                        </div>
                        <div className={classes.actionButtonContainer}>
                            <Button
                                className={classes.button}
                                classes={{
                                    label: classes.label,
                                }}
                                href={event ? event.calendlyUrl : ''}
                                target="_blank"
                                onClick={() => sendGaEvent("Contact", "User Click", "Appointment Scheduler")}
                            >
                                Let's Chat
                            </Button>
                            
                        </div>
                    </Grid>
                </Grid>

                {
                    event_details_content ?
                    event_details_content.items.map(item => 
                        <Item 
                            {...item} 
                            sendGaEvent={sendGaEvent} 
                            linkedAssets={event_details_content.linkedAssets} 
                        />
                    )
                    : <div />
                }    
            </div>
            
        </div>
    );
};

const mapStateToProps = state => {
    return {
        events_content: state.events_content,
        event_details_content: state.event_details_content,
    };
};

export default connect (
    mapStateToProps,
    {
        fetchEvents,
        fetchEventDetailitems,
    }
)(EventDetailList);