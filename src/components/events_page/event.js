import React from 'react';

import { 
    makeStyles,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography,
    Grid,
    Paper,
    Button,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import NewReleasesIcon from '@material-ui/icons/NewReleases';

import { Link as RouterLink } from "react-router-dom";

import Calendly from './calendly';
import PromotionFeed from './promotionFeed';


const useStyles = makeStyles(theme => {
    const containerHeight = 500;
    return ({
        root: {
            width: '100%',
            marginTop: 100,
        },
        expansionPanelRoot: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'center',
            flexWrap: 'wrap',
        },
        headingContainer: {
            width: 275,
        },
        heading: {
            fontSize: 20,
            [theme.breakpoints.down('sm')]: {
                fontSize: 15,
            },
            fontFamily: "\"Cabin\", sans-serif",
        },
        subHeading: {
            fontSize: 20,
            fontFamily: "\"Cabin\", sans-serif",
            [theme.breakpoints.down('sm')]: {
                fontSize: 15,
            }

        },
        column: {
            flexBasis: '66.66%'
        },
        headingPromptTableContainer: {
            width: '100%',
        },
        headingPromptContainer: {
            marginLeft: 'auto',
            color: "#66c34a",
            width: 175,
            verticalAlign: 'middle',
        },
        headingPrompt: {
            verticalAlign: 'middle',
            width: 155,
            fontFamily: "\"Cabin\", sans-serif",
        },
        headingPromptBang: {
            margin: 'auto',
            verticalAlign: 'middle',
        },
        primaryContent: {
            width: '100%',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        titleContainer:{
            width: '100%',
            backgroundColor: '#474747',
            color: '#FAFAFA',
            height: containerHeight,
            position: 'relative',
            paddingLeft: 2,
            paddingRight: 2,
            marginBottom: 2,
        },
        title: {
            position: 'absolute',
            top: "10%",
            width: '85%',
            marginLeft: 5,
            fontFamily: "\"Raleway\", sans-serif",
            fontSize: 50,
        },
        subTitle: {
            position: 'absolute',
            top: "70%",
            width: '80%',
            marginLeft: 5,
            fontFamily: "\"Cabin\", sans-serif",
        },
        schedulerContainer: {
            width: '100%',
            height: containerHeight,
            paddingLeft: 2,
            paddingRight:2,
        },
        introVideoContainer:{
            width: '100%',
            textAlign: 'center',
        },
        introVideo: {
            width: '100%',
            maxHeight: 700,
            padding: 10,
            [theme.breakpoints.down('sm')]: {
                padding: 1,
            },
        },
        introVideoCaption: {
            width: '100%',
            textAlign: 'center',
            paddingTop: 10,
            paddingBottom: 30,
            fontFamily: "\"Cabin\", sans-serif",
        },
        secondaryContent: {
            width: '100%',
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        actionContainer: {
            width: '100%',
            // height: containerHeight,
            height: 500,
            position: 'relative',
        },
        actionDescriptionContainer: {
            width: '90%',
            minHeight: 500,
            position: 'absolute',
            top: '10%',
        },
        actionDescription: {
            width: '100%',
            maxWidth: 350,
            fontFamily: "\"Cabin\", sans-serif",
            textAlign: 'justify',
            marginRight: 'auto',
            marginLeft: 'auto',
        },
        actionButtonContainer:{
            width: '90%',
            position: 'absolute',
            top: '75%',
            textAlign: 'center',
        },
        actionButton: {
            background: 'linear-gradient(45deg, #66c34a 30%, #00A3E0 90%)',
            border: 0,
            borderRadius: "20px 0px 20px 0px",
            boxShadow: '0 3px 5px 2px rgba(103, 199, 172, .3)',
            color: 'white',
            height: 55,
            padding: '0 30px',
            paddingTop: 15,
            display: 'inline-block',
            verticalAlign: 'middle',
            margin: 4,
        },
        descriptionContainer: {
            width: '100%',
            minHeight: 500,
            backgroundColor: '#474747',
            color: '#FAFAFA',
        },
        descriptionTextContainer: {
            height: '100%',
            padding: 15,
            marginLeft: 'auto',
            marginRight: 'auto',
            fontFamily: "\"Cabin\", sans-serif",
            fontSize: 18,
        },
        logoContainer: {
            width: 225,
            height: '100%',
            marginLeft: 'auto',
            marginRight: 'auto',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
        logo: {
            width: "100%",
        }
        ,
    })
})


const Event = props => {
    const classes = useStyles();
    const { 
        index,
        expanded,
        handleSetExpanded,
        sendGaEvent,
        promotion_items,
    } = props;
    const { 
        id,
        title, 
        subTitle,
        introVideoCaption,
        startDate,
        endDate,
        dateText,
        description,
        actionDescription,
        calendlyUrl,
        introVideoUrl,
        posterImageUrl,
        slug,
    } = props.item;

    const [happeningNow, setHappeningNow] = React.useState(true);
    const [happeningSoon, setHappeningSoon] = React.useState(false);

    React.useEffect(() => {
        const today = new Date();
        let nextWeek = new Date();
        nextWeek.setDate(today.getDate() - 7)

        setHappeningNow(today >= startDate &&  today <= endDate ? true : false)
        setHappeningSoon(today <= startDate && nextWeek >= startDate ? true : false)
    }, [startDate, endDate])

    const getEventPrompt = () => { 
        if (happeningNow) {
            return "Happening Now"
        } else if (happeningSoon) {
            return "Happening Soon"
        }
    }

    return (
        <ExpansionPanel expanded={expanded === index} onChange={handleSetExpanded(index)}>
            <ExpansionPanelSummary
                expandIcon={<ExpandMoreIcon />}
                aria-controls={`panel${index}-content`}
                id={`panel${index}-header`}
                onClick={() => sendGaEvent("Event Expansion Pannel", "User Click", id)}
            >
                <Grid container spacing={1}>
                    <Grid item sm={6} className={classes.headingContainer}>
                        <Typography variant="subtitle1" className={classes.heading} >{`${title}`}</Typography>
                    </Grid>
                    <Grid item sm={6} className={classes.headingContainer}>
                        <Typography variant="subtitle1" className={classes.subHeading}>{`${dateText}`}</Typography>
                    </Grid>
                </Grid>
                
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <Grid container spacing={0} className={classes.primaryContent}>
                    <Grid item sm={12} className={classes.headingPromptTableContainer}>
                        <table className={classes.headingPromptContainer}>
                            <td>{happeningNow || happeningSoon ? <NewReleasesIcon className={classes.headingPromptBang} /> : <div />}</td>
                            <td><Typography variant="h6" className={classes.headingPrompt}>{getEventPrompt()}</Typography></td>
                        </table>
                    </Grid>
                    <Grid item sm={6} className={classes.titleContainer}>
                        <Typography variant="h3" className={classes.title}>{title}</Typography>
                        <Typography variant="h6" className={classes.subTitle}>{subTitle}</Typography> 
                    </Grid>
                    <Grid item sm={6} className={classes.schedulerContainer} >
                        <Calendly calendlyUrl={calendlyUrl}/>
                    </Grid>
                </Grid> 
            </ExpansionPanelDetails>
            <ExpansionPanelDetails>
                <Paper className={classes.introVideoContainer}>
                    <video 
                        className={classes.introVideo} 
                        poster={posterImageUrl ? posterImageUrl : undefined} 
                        controls
                        onPlay={() => sendGaEvent("Event Intro Video", "Play", id)}
                    >
                        <source src={introVideoUrl} />
                    </video>
                    <Typography variant="h6" className={classes.introVideoCaption} >{introVideoCaption}</Typography>
                </Paper>
            </ExpansionPanelDetails>
            <ExpansionPanelDetails>
                <Grid container spacing={4} className={classes.secondaryContent} >
                    <Grid item sm={6} className={classes.actionContainer}>
                        <div className={classes.actionDescriptionContainer}>
                            <Typography className={classes.actionDescription} variant="subtitle1">
                                {actionDescription ? actionDescription : "" }
                            </Typography>
                        </div>
                        <div className={classes.actionButtonContainer}>
                            <Button 
                                component={RouterLink}
                                to={`/events/${slug}`}
                                className={classes.actionButton}
                            >
                                Learn More
                            </Button>
                        </div>
                    </Grid>
                    <Grid item sm={6} className={classes.descriptionContainer}>
                        <div className={classes.descriptionTextContainer}>
                        { 
                            promotion_items && promotion_items.filter(item => item.eventId === id).length > 0 ?
                            <PromotionFeed  items={
                                promotion_items
                                    .filter(item => item.eventId === id)
                                    .sort((a, b) => b.publicationDate - a.publicationDate)
                                    .slice(0,3)
                                }
                            />
                            : 
                            <div className={classes.logoContainer}> 
                                <img className={classes.logo} src="https://images.ctfassets.net/m7umyxqhfu1m/1KZRWCsAH8qcSzGWaH5U94/aa548a394b0ed9b70c258d1dac1bac64/TCARTA_Swoop_white.png?h=250" alt="TCarta Logo" />
                            </div>
                        } 
                        </div>
                    </Grid>
                </Grid>
            </ExpansionPanelDetails>
        </ExpansionPanel>
    )
};

export default Event;