import React from 'react';

const Calendly = (props) => {
    const { calendlyUrl } = props;
    return (
            <iframe
                src={calendlyUrl}
                width="100%"
                height="100%"
                frameBorder="100%"
                title="Calendly event scheduler"
            ></iframe>
    )
};

export default Calendly;