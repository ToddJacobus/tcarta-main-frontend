import React from 'react';
import { connect } from 'react-redux';
import { 
    makeStyles,
    Typography,
} from '@material-ui/core';

import ReactGA from 'react-ga';

import {
    fetchEvents,
    fetchFutureEvents,
    fetchEventPromotions,
} from '../../actions';

import Event from './event';
import FutureEvent from './futureEvent';

const useStyles = makeStyles(theme => {
    return ({
        root: {
            width: '100%',
            marginTop: 50,
            maxWidth: 1200,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
        
        bannerImage: {
            width: '100%',
            maxWidth: 1200,
            borderRadius: 5,
        },
        headerContainer: {
            marginLeft: 20,
            marginTop: 10,
            marginBottom: 45,
        },
        header: {
            fontFamily: "\"Raleway\", sans-serif",
            [theme.breakpoints.down(450)]: {
                fontSize: '10vw',
            },
        },
    })
})

const EventsPage = props => {
    const classes = useStyles();
    const { 
        events_content, 
        fetchEvents,
        future_events_content,
        fetchFutureEvents,
        handleNavbarIndicatorChange,
        fetchEventPromotions,
        event_promotion_content,
    } = props;
  
    const [expanded, setExpanded] = React.useState(0);

    const handleSetExpanded = panel => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    React.useEffect(() => {
        fetchEvents();
        fetchFutureEvents();
        fetchEventPromotions();
    }, [fetchEvents, fetchFutureEvents, fetchEventPromotions]);

    React.useEffect(() => {
        handleNavbarIndicatorChange(2);
    }, [handleNavbarIndicatorChange])

    const [hasViewed, setHasViewed] = React.useState(false);
    React.useEffect(() => {
        if (!hasViewed) {
            const locationUrl = window.location.pathname + window.location.search;
            ReactGA.ga('set', 'page', locationUrl);
            ReactGA.pageview(locationUrl);
            setHasViewed(true);
        };
    }, [hasViewed]);

    return (
        <div className={classes.root}>
            <div>
                <img 
                    src={events_content ? events_content.items[0].bannerImageUrl : "#" } 
                    alt="Banner"  
                    className={classes.bannerImage}
                />
                <div className={classes.headerContainer}>
                    <Typography variant="h3" className={classes.header}>
                        What we're up to
                    </Typography>

                </div>
            </div>
            {
                future_events_content && future_events_content.total > 0 ?
                future_events_content.items.map((item, index) => (
                    <FutureEvent item={item} key={item.id} {...props} />
                ))
                : <div />
            }
            {
                events_content?
                    events_content.items.map((item, index) => 
                        <Event 
                            item={item}
                            promotion_items={event_promotion_content ? event_promotion_content.items: []}
                            index={index} 
                            expanded={expanded} 
                            handleSetExpanded={handleSetExpanded} 
                            key={item.id}
                            {...props}
                        />
                    )
                : <div />
            }

        </div>
    )
}

const mapStateToProps = state => {
    return {
        events_content: state.events_content,
        future_events_content: state.future_events_content,
        event_promotion_content: state.event_promotion_content,
    }
};

export default connect(
    mapStateToProps,
    {
        fetchEvents,
        fetchFutureEvents,
        fetchEventPromotions,
    }
)(EventsPage);