import axios from 'axios';

export default axios.create({
    baseURL: 'https://authenticate.tcarta.com/api'
})