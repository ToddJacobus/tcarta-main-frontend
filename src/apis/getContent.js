import axios from 'axios';

export default axios.create({
    baseURL: 'https://cdn.contentful.com/spaces/m7umyxqhfu1m/environments/prod-1/entries'   // Production base URL
})